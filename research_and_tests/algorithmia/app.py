import Algorithmia

apiKey = ""
client = Algorithmia.client(apiKey)



input = {
    "image": ""
  
}
algo = client.algo('tofriede/FashionSegmentation/0.1.5')
algo.set_options(timeout=300)  # optional
print(algo.pipe(input).result)
