# Fashion AI
## HES-SO Master thesis - Flavia Pittet

### Introduction

What happens when you combine fashion and machine learning? The fashion industry has recently been benefiting from such technologies, but a lot of possibilities still remain. Whether used for style matching, recommendation systems, or trend predictability, there is no doubt regarding its grand potential in this market. 

The customer can easily get lost between all the trends and new clothing items on the market."What fits me?", "What goes with this skirt?", "If I buy this dress, which of my shoes would go with it?" are several questions that could be answered with Machine Learning.

Our approach is to create a fashion conscious algorithm and application which can score a person’s outfit based on current trends. This project’s scope includes the development of such and what it entails, such as data collection, analysis and normalization, as well as testing and implementation.

### Structure

- *backend*: contains the API and ML model ready to use
- *extensive testing module*: tests of two existing algorithms on how they perform in identifying clothing items on an image. Dataset used: Chictopia
- *ML_module*: python notebook of Mobilnet with transfert learning (using Cuda)
- *research and tests*: all tests and existing modules tested
- *telegram_bot*: bot to interact with API (send image and get score)
- *Thesis (Latex)*: full Latex thesis and pdf

### Citation and usage

Feel free to build on top of my work, but please cite me.

```
@misc{pittet_2020, url={https://gitlab.com/flavz27/master-thesis-fashionai}, 
publisher={HES-SO, Switzerland}, author={Pittet, Flavia}, year={2020}, month={Apr}}
```