require('dotenv').config()
const Telegraf = require('telegraf')
const Markup = require("telegraf/markup")
const session = require('telegraf/session')
const Stage = require('telegraf/stage')
const Scene = require('telegraf/scenes/base')
const WizardScene = require('telegraf/scenes/wizard');

const axios = require('axios')
const FormData = require('form-data')

const API_URL = process.env.BACKEND_URL

const TEST_API_URL = process.env.TEST_BACKEND_URL
// const API_URL = TEST_API_URL

// Start Scene
const startScene = new Scene('start')
startScene.enter((ctx) => ctx.reply(`Welcome to the FashionAI bot! Start by sending a picture of yourself wearing an outfit. Make sure we can see all of you, from head to toes !`))
startScene.on('photo', (ctx) => {
    //Get image from Telegram servers
    const file_id = ctx.message.photo[0].file_id
    ctx.telegram.getFileLink(file_id).then(url => {
        // Send image to Backend
        axios({url, responseType: 'arraybuffer'}).then(response => {
            const filename = url.split("/").pop()
            const image = new Buffer.from(response.data)
            const formData = new FormData()
            formData.append('image', image, {filename: filename})

            ctx.reply(`Thank you ! We will analyze the image and get back to you with a list of clothing items detected from the image`)
            axios.post(API_URL + '/image', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data; boundary=' + formData._boundary,
                }
            })
            .then(function (response) {
                if(!!response.data && !!response.data.articles && !!response.data.req_id) {
                    ctx.session.data = response.data

                    const list = (response.data.articles.length === 0) ? 'No items identified. 😭' : response.data.articles.reduce(function(acc, val, idx) {
                        const newLine = idx < (response.data.articles.length-1) ? '\n- ' : ''
                        return acc + val + newLine
                    }, '- ')
                    ctx.reply('Here is the list of the items we have identified. Is this correct ?\n\n' + list,
                        Markup.inlineKeyboard([
                            Markup.callbackButton("Yes", "get-score"),
                            Markup.callbackButton("No, and update", "ask-correction")
                        ]).extra()
                    )
                }
            })
            .catch(function (error) {
                console.log(error.response)
                if(!!error.response.data && !!error.response.data.error) {
                    ctx.reply(error.response.data.error+" 😭 Try with another image !")
                }
                else {
                    console.log(error)
                    ctx.reply('Sorry, error with backend')
                }
            })
        })
        .catch(function (error) {
            console.log(error)
            ctx.reply('Sorry, we couldn\'t get your file 😢')
        })
    })
})
startScene.on('text', (ctx) => ctx.replyWithMarkdown('Send an `image` for us to analyse !'))

const correctList = new WizardScene('correct-list',
    (ctx) => {
        ctx.reply(`Please write a list of the clothing pieces and accessories worn in the picture separated by commas. For example:`)
            .then(() => ctx.reply(`bag,shoes,earrings,shorts`))
        return ctx.wizard.next()
    },
    (ctx) => {
        const new_list = ctx.message.text.split(',')
        if(new_list.length > 0) {
            ctx.session.data.articles = new_list
            ctx.scene.enter('compute-score')
        }
        else {
            ctx.reply(`Can't parse your list, sorry 😭`)
        }

        return ctx.scene.leave()
    }
)

const computeScene = new Scene('compute-score')
computeScene.enter(
    async (ctx) => {
        ctx.reply(`Thank you for completing the information ! We will calculate a score for your outfit very quickly with our magic servers`)
        await axios.post(API_URL + '/getScore/' + ctx.session.data.req_id, {"articles": ctx.session.data.articles}, {
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(function (response) {
            if(response.data.score !== undefined) {
                ctx.reply(`Your outfit has been rated a: ${response.data.score}/10`)
            }
            return ctx.scene.leave()
        })
        .catch(function (error) {
            console.log(error.response)
            if(!!error.response.data && !!error.response.data.error) {
                ctx.reply(error.response.data.error)
            }
            else {
                ctx.reply('Sorry, we couldn\'t get your score 😢')
            }
        })
    }
)


const bot = new Telegraf(process.env.BOT_TOKEN)
const stage = new Stage([startScene, correctList, computeScene], { ttl: 30 })

stage.action('get-score', (ctx) => {
    console.log('enter in get-score')
    if(!!ctx.session.data) {
        ctx.scene.enter('compute-score')
    }
    else {
        ctx.reply(`Session timed out`)
    }

})
stage.action('ask-correction', (ctx) => {
    console.log('enter in ask-correction')
    if(!!ctx.session.data) {
        ctx.scene.enter('correct-list')
    }
    else {
        ctx.reply(`Session timed out`)
    }
})

bot.command('test', (ctx) =>{
    axios.get(API_URL + '/')
    .then(function (response) {
        if(!!response.data) {
           
            ctx.reply('Backend running')
        }
    }).catch(function (error) {
        console.log(error)
        ctx.reply('Sorry, no back-end running')
    })
})

bot.use(session())
bot.use(stage.middleware())
bot.command('start', (ctx) => ctx.scene.enter('start'))

bot.on('message', (ctx) => ctx.reply('Try /start or /help'))

bot.launch()
console.log(`Bot launched.`)