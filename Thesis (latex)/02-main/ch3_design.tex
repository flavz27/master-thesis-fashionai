\chapter{Conception and design}
\label{chap:design}
\section{Architecture}
\subsection{Requirements}
The final product must meet the following requirements:

\begin{itemize}
    \item send and upload an image to be analysed
    \item return a score
    \item return a list of clothing items that can be edited by the user
    \item store all this data (IP address, score, list of clothing, image)
    
\end{itemize}

\subsection{Front-end}
To do so, the decision was made to create a Telegram Bot \footnote{"Bots are third-party applications that run inside Telegram. Users can interact with bots by sending them messages, commands and inline requests. You control your bots using HTTPS requests to our Bot API." - core.telegram.org/bots} which would allow the user to interact with a bot as if it were chatting with a human being. This would allow a faster development time of the front-end to concentrate of the back-end aspect.
\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.5\columnwidth]{02-main/figures/botfather.png} 
	\captionsource{The Telegram Botfather}{The Telegram Botfather visual }{core.telegram.org}
	\label{fig:telegrambotfather} 
\end{figure}
Bots are highly customizable while still maintaining easy development tools. As a developer, this means that we can create an interactive Front-end by using the Telegram messaging system. Potential users might already have the application downloaded which makes the testing process even better. No \acrlong{apk} needs to be installed on a smartphone, which drastically limits the \acrshort{spof}s in the development and deployment processes.

\subsection{Back-end}
The Back-end is responsible for receiving and storing incoming images to be analysed, as well as computing whether a person is present on the image, what clothing items are worn and the final outfit score.

Thus, the image needs to go through several modules:
\begin{itemize}
    \item \gls{openpose}: if a human is detected, returns the keypoints (face, hands, feet, etc.)
    \item \gls{fashionsegmentation}: returns a list of worn items (headwear, pants, footwear, etc.)
    \item FashionAI: returns a score based on an image, and ideally, based on a list of worn items for extra data.
\end{itemize}

All these modules are steps and/or gates in the process. The first step is \gls{openpose}. It detects the human body's keypoints. This model has been deployed as a microservice on the \gls{icossys} servers and is available to use as an API to detect if a person is present in the image. This also allows us to determine if enough of the person is visible to be able to detect clothing items.

For example, \gls{openpose} might detect the presence of a human being, but only the face and shoulders. No outfit can be identified based on this only. As \gls{openpose} returns keypoints, we can define a list of necessary points that must be present for the image to go through the gate onto the next step.

\begin{lstlisting}[language=Json, caption=Sample return data from the icoservices API]
{
    "data": {
        "image": null,
        "people": [
            {
                "body": [
                    {
                        "confidence": 0.9136375188827515,
                        "key": "Nose",
                        "points": [
                            192.51307678222656,
                            135.71937561035156
                        ]
                    },
                    {
                        "confidence": 0.6958087682723999,
                        "key": "Neck",
                        "points": [
                            192.52154541015625,
                            189.14199829101562
                        ]
                    },
                   {...},
                    {
                        "confidence": 0.0,
                        "key": "RHeel",
                        "points": [
                            0.0,
                            0.0
                        ]
                    }
                ],
                "face": null,
                "hands": [
                    null,
                    null
                ]
            }
        ]
    }
}
\end{lstlisting}


\gls{fashionsegmentation} is the second gate. This model detects items worn in images and returns the bounding boxes and probability, as seen in \fullref{chap:validation}. It also gives a segmentation per item as a polygon. The bounding box in the rectangle around the image, while the segmentation is the coloured portion of the item in Figure \ref{fig:fashionsegexample}
\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.7\columnwidth]{02-main/figures/fashion_segmentation_eg.jpg} 
	\captionsource{fashion segmentation}{ Fashion Segmentation detection algorithm example}{algorithmia.com/algorithms/tofriede/FashionSegmentation/docs}
	\label{fig:fashionsegexample} 
\end{figure}
If the image has been validated at the first step, it is sent to the \gls{algorithmia} servers to be analysed by \gls{fashionsegmentation}. 

\begin{lstlisting}[language=Json, caption=Return data structure of Fashion Segmentation]
[
  {
    "article_name": String,
    "bbox": [
        Integer, //y1
        Integer, //x1
        Integer, //y2
        Integer, //x2
    ],
    "confidence": Float,
    "segmentation": [[
        Integer, //y1
        Integer, //x1
        Integer, //y2
        Integer, //x2
        Integer, //y3
        Integer, //x3
        Integer, //y4
        Integer, //x4
        Integer, //y5
        Integer, //x5
    ]]
  },
  ..
]
\end{lstlisting}

The data is then stored onto a \gls{mongodb}. The image itself is saved on a \gls{minio} Object Storage. The clothing items can be modified by the user during the bot interaction. This data can be used to re-create a new dataset with new images and their respective clothing items.

The last step is FashionAI. This model should be trained on images with scores and worn items as a ground truth to predict the score of an image (c.f. \fullref{chap:implementation}).

All of these steps help construct a fully robust architecture.
\subsection{Architecture Diagram}
\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.85\columnwidth]{02-main/figures/architecture.png} 
	\caption{Architecture Diagram}
	\label{fig:architecturediagram} 
\end{figure}

\section{\acrshort{uml}}
Creating an \acrshort{uml} Sequence Diagram to specify the communication between the telegram Bot and the back-end was necessary to define the data being sent.

The \incode{alt} is on the user's side. The choice to modify the list of items is a user input, explaining why the \gls{uml} diagram includes a choice on the client's side and alternative routes.
\begin{figure}[H] 
	\centering 
	\includegraphics[width=1.2\columnwidth]{02-main/figures/uml_backend.png} 
	\caption{UML Sequence Diagram between the API and the client }
	\label{fig:uml_seq_diag} 
\end{figure}

The API has two endpoints; \incode{/api/image} and \incode{/api/getScore/<RequestID>} in POST. 

The first endpoint allows the client to upload the image. It is saved in the back-end. The API then generates a RequestId (\acrshort{uuid}) which corresponds to the image ID. This route returns the RequestId and the list of clothing items that have been detected. If no human was detected in the image, or if no clothing was found, it returns an error with a specific message in Json and the corresponding HTTP error code. 

The second endpoint uses the RequestId to retrieve the image and return a score.

% Following the research and analysis stage, a "module driven" architecture has been adopted. This way, all the inputs and outputs of each system can clearly be identified. 

% \begin{figure}[H] 
% 	\centering 
% 	\includegraphics[width=1.1\columnwidth]{img/fashionAiArchitecture.jpg} 
% 	\captionsource{FashionAI Architecture}{ FashionAI Architecture diagram }{draw.io tool}
% 	\label{fig:fashionaiarchi} 
% \end{figure}
% The first module group (Human picture extraction) receives an image and outputs a bounding box of the detected person. Thus allowing the several following modules to use this image which only contains the necessary information.

% Following the bounding box, two types of modules will come into play:
% \begin{itemize}
%   \item \textbf{The body identification modules}.
%     The goal of the two modules (body type and traits identification modules) is to create a virtual representation of the person. Depending on the hair color, length or body type, certain clothes may not be suited, thus our need for this identification.
%   \item \textbf{The clothes and accessories identification}.
%   This module allows us to retrieve a list of clothes and accessories worn in the picture. Cross referenced with a database, this will allow our Fashion Conscious module to evaluate the outfit.
% \end{itemize}

% \section{Body identification modules}
% Determining a person's body type is important to rate an outfit. Depending on your figure, certain items of clothing might not be recommended. The hair, eye and skin colour are also factors that should be taken into account when determining the right outfit. A long yellow dress will not fit a blonde, light skin woman. But it might fit a brunette with darker skin better.

% The questions stands as to how to determine the body type, hair, eye and skin color and how to detect it. The following chapters tried to answer these questions.

% \subsection{Body calculator}
% Throughout our research, we have found that 
% \begin{enumerate}
%     \item body types calculations differ from women to men. Meaning that our module will have to know if it is calculating for a man or a woman. Men have five types; oval, triangle, rectangle, rhomboid, and inverted triangle\footnote{metro.co.uk/2017/05/21/there-are-only-five-male-body-shapes-according-to-health-experts-6650097/}. Whereas women have four; inverted triangle, rectangle, triangle and hourglass \footnote{whowhatwear.co.uk/how-to-find-body-shape-calculator}.
%     \item there is no consensus for women body type. More information in \autoref{chap:difficulties} Difficulties.
% \end{enumerate}

% \textbf{Data "In"}: bounding box image

% \textbf{Data "Out"}: body type (triangle, hourglass, inverted triangle, rectangle)

% In order to facilitate the body type identification process, the user must input whether they are male or female). This makes the process much easier and requires less hassle. 

% \subsection{Traits identification module}
% Following our research, we have identified a paper on hair segmentation and color extraction from images \cite{hair}. Their approach and model can be implemented in this module.

% \textbf{Data "In"}: bounding box image

% \textbf{Data "Out"}: Hair colour, eye colour, skin colour

% \section{Clothes and accessories identification module}
% The goal of this module is to identify each item worn by the person detected in the picture. As seen in the \ref{relatedWork} Related work section, one paper mentions the use of CNNs to automatically tag items on an image \cite{deeplearning}. They have managed to get an accuracy of around 90\%.

% The ideal case would be to identify a specific item of clothing with its brand and all related meta data. 

% For example, instead of detecting: \incode{ ["blue jeans", 
% "white polo shirt","white shoes"]}. The system could recognize the following: \incode{["blue jeans boot-cut Diesel", "white polo shirt Lacoste",
%     "converse white shoes"]} which provides more information regarding the general look and item compatibility.

% \begin{figure}
% \centering
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{img/polo_white_ralph_lauren.jpeg}
%     \captionsource{Ralph Lauren White polo}{ Ralph Lauren White polo }{www.ralphlauren.com}
%     \label{fig:poloralph}
%   \label{fig:test1}
% \end{minipage}%
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{img/polo_white_lacoste.jpg}
%     \captionsource{Lacoste White polo}{Lacoste White polo }{www.elcorteingles.com}
%     	\label{fig:pololacoste}
%   \label{fig:test2}
% \end{minipage}
% \end{figure}

% This can be done by identifying signature patterns from certain brands, whether located on tops like our example, or other specialties. For example, for the Lacoste brand, the crocodile can be identified. For the Ralph Lauren brand, the polo player can also be identified.

% To illustrate this, \autoref{fig:poloralph} and \autoref{fig:pololacoste} both contain Polo shirts that look very similar. The brand can only be determined by looking at the logo on the upper-right of the item.

% \textbf{Data "In"}: bounding box image

% \textbf{Data "Out"}: list of items with their attributes (colour, size, texture, description, brand, etc.)


% \section{Fashion conscious module}
% The Fashion Conscious module is the most important part of this application. It takes all the outputs from the previous modules and can give advice on how to improve your look. 

% It will be able to tell you that if you are wearing a large yellow dress as a small and blonde woman, it will not showcase your best features. However, if you are a tall, darker skin brunette, the Fashion Conscious module will tell you that your outfit suits you. Context matters. 

% This context and fashion aware module can be developed using a CNN. The biggest difficulty in creating such a system is the data-set. Finding and/or collecting data containing all the information needed is close to impossible without spending a lot of time and money.

% % -----------------------------------------------------------------------------
% %\section{Short lists}

% %\blindtext

% %\blindlist{enumerate}

% %\blindtext

% %\blindtext

% %\blindlist{itemize}

% %\blindtext

% %\blinddescription

% % -----------------------------------------------------------------------------
% %\section{Long lists}

% %\blindtext

% %\Blindlist{enumerate}

% %\blindtext

% %\Blindlist{itemize}

% %\blindtext

% %\Blinddescription

% % -----------------------------------------------------------------------------
% %\section{Multi-level lists}

% %\blindtext

% %\blindlistlist[3]{itemize}

% %\blindtext

% %\blindlistlist[3]{enumerate}

% % -----------------------------------------------------------------------------
% \section{Conclusion}
% The whole conception and design of this project has been an interesting task, taking in different business oriented inputs while looking at technical capabilities.

% The modular segmentation of the project's architecture is necessary as it allows us to segment our code and have a clear vision of inputs and outputs for each section of the application.
