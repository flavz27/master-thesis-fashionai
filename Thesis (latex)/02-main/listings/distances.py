shoulders = abs(points[BODY_PARTS["LShoulder"]][0] - points[BODY_PARTS["RShoulder"]][0])
hips = abs(points[BODY_PARTS["LHip"]][0] - points[BODY_PARTS["RHip"]][0])

print('Shoulders: '+ str(shoulders), 'Hips: ' + str(hips))

shouldersL = np.array((points[BODY_PARTS["LShoulder"]][0] ,points[BODY_PARTS["LShoulder"]][1]))
shouldersR = np.array((points[BODY_PARTS["RShoulder"]][0] ,points[BODY_PARTS["RShoulder"]][1]))

shouldersEucl = np.linalg.norm(shouldersL - shouldersR)

hipsL = np.array((points[BODY_PARTS["LHip"]][0] ,points[BODY_PARTS["LHip"]][1]))
hipsR = np.array((points[BODY_PARTS["RHip"]][0] ,points[BODY_PARTS["RHip"]][1]))

hipsEucl = np.linalg.norm(hipsL - hipsR)

print('Shoulders Eucl: '+ str(shouldersEucl), 'Hips Eucl: ' + str(hipsEucl))