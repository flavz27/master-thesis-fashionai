\chapter{Research and analysis}
\label{chap:analysis}

In order to create and implement the best possible solution, research must be conducted on existing works.


\section{Existing works}

Several works are relevant to this work. The most interesting one is called Fashion++ \cite{fashionplusplus}. Their Generative Model is able to take an image of a person wearing an outfit as an input, and generate a new one with small changes to improve it overall. The important part of this work was to understand what type of dataset was used to train their model. How do you define a fashionable outfit ? 

Their approach was very helpful to understand how they acquired their data. They were using the Chictopia dataset \cite{grahamar}. The original website, chictopia.com \cite{chictopia}, was a  fashion social network where users could publish pictures of themselves wearing outfits. Other users could then comment, favorite or like their posts. The dataset was created based on the pictures published on this website which consisted of approximately 100'000 images. They used these images to alter outfits and replacing items in images.

While researching possible ways to identify clothing items in an image, two pre-trained models were found as a callable service on \gls{algorithmia}. DeepFashion and Fashion Segmentation \cite{sutton} are two algorithms that analyze an image, and return a list of clothing. DeepFashion mentioned that their work was created in collaboration with the authors of another paper \cite{griebel2019picture}. Both of these models need to be tested as no information regarding their accuracy rate has been published (see \fullref{chap:validation}).

\section{Datasets}
Finding a dataset containing all the desired information is difficult. The necessary information needed to predict an outfit's fashionability score are the following:
\begin{itemize}
    \item images of clothes being worn (outfit)
    \item items being worn (type, brand, colour, bounding box)
    \item fashionability score per image/outfit
\end{itemize}

Thus, a dataset which offers all three data points is nonexistent, but can be assembled.

The first approach to create this dataset was to find a website with images of people posting their outfits, under a creative commons license to be able to use said images, such as \gls{flikr}. Then, the tedious efforts to annotate all images would have been necessary. 

Nevertheless, a much better alternative was found with the help of the Fashion++ paper \cite{fashionplusplus}. Their approach was using the Chictopia dataset \cite{chictopia} as a starting point. 


By taking this dataset, we already have the following information:
\begin{itemize}
    \item 100'000 images of people in outfits
    \item list of items being worn in the images 
    \item information regarding the post it comes from (likes, votes and comments)
\end{itemize}

The data was collected in 2011, it contains approximately 100'000 images depending on the availability as users can still remove their posts and images. The dataset includes a python script to download the images on the Chictopia website.

We can utilize the social information per outfit, such as likes, votes and comments, to approximate an outfit's fashionability score. The more attention it gets, the more likeable it must be. This approach is cited in the Fashion++ paper \cite{fashionplusplus}.

Ideally, more work needs to be done on the items individually to fill in all the missing data, such as:
\begin{itemize}
    \item clothing/item colour: getting an average RGB code of a bounding box per item is a way to get an approximation of the colour. However, the differences in lighting and multi-color items may not be recognized correctly, and some manual input should be implemented to ensure the accuracy of the data. 
    \item clothing/item brand: certain items of clothing show off their brand in specific places of their clothing items. For example, most polo T-shirts show the brand's logo near the left pocket, such as Polo Ralph Lauren\footnote{www.ralphlauren.com} and Lacoste \footnote{www.lacoste.com}. These icons can be easily identified and matched with their respective brands.
    \item bounding box of detected clothing and items: this part of the data completion can be partially done with the Algorithmia algorithms tested in \fullref{chap:validation}. Although some manual input is needed as the algorithms are not completely accurate. 
\end{itemize}

With this in mind, aggregating all the data points needed to create our dataset is possible. 


\chapter{Testing and Validation of existing Models}
\label{chap:validation}

% \section{Testing} \todo{evaluation of models} evaluation of existing models

Following the research, two Algorithmia services were potentially able to identify clothing items in images, Deep Fashion and Fashion Segmentation. These pre-trained models are hosted on the Algorithmia servers and offer an API to use these models. Henceforth, the testing process can be done on any machine without the need for computing power or initial set-up and compiling.

Given that both algorithms must offer different results, testing must be done to determine the best possible detection scores on our Chictopia dataset \cite{chictopia}.



\section{Dataset preparation}
As mentioned, the Chictopia dataset \cite{chictopia} is the ideal candidate to test the two algorithms. 

Three different types of image categories were created:
\begin{itemize}
    \item Ideal cases [150 images]: containing images with good light exposure, only one person in the image, a neutral background and top to bottom models.
    \item Realistic cases [111 images]: containing images with lower quality light exposure, cropped models or multiple subjects in the image. Mostly, the "selfie in the mirror" type of images were included.
    \item Extreme cases [10 images]: containing images with lower resolution, bad exposure, negative images and black and whites. 
\end{itemize}

\begin{figure}
\centering
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{02-main/figures/datasetimgs/ideal_case.jpg}
    \centering
    \subcaption{ Ideal case}

\end{minipage}%
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/datasetimgs/realistic_case.jpg}
        \subcaption{ Realistic case}

\end{minipage}
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/datasetimgs/extreme_case.jpg}
        \subcaption{ Extreme case}
\end{minipage}
\caption{Examples of images from their respective categories}
\end{figure}

The ideal cases provides us with an idea of the algorithm's potential under perfect circumstances. Whereas the realistic cases give us more of an overview on how the algorithms will perform in real life scenarios. Finally, the extreme cases were used to grasp the limits of the algorithms, and whether changing certain aspects of an image, such as the resolution, light exposure, or colour would affect the results. 

These extreme cases were chosen at the very end of the tests. The images from the Ideal and Realistic cases categories that were correctly identified by the two algorithms without modification were processed. This way, the change in performance could only be due to the image's modification, and no other factor. 

\section{Scripts and dataset scrapping}
The Fashionista dataset gives us a list of items worn per image in a \incode{.txt} file. With this in mind, some processing has to be done to create a \incode{.json} file with all images, per category, with their respective articles worn and images identifiers.

Social information like number of comments, votes or likes per post need to be extrapolated for each image from the fashionista website. 

As a result, several python scripts have been created to process images, and their data.
\begin{figure}[H]
    \centering
    \includegraphics[width=1.2\linewidth]{02-main/figures/testing_diagram.png}
    \caption{Testing steps diagram}
    \label{fig:testingDiagram}
\end{figure}
The first step is to grab all the selected images from the three categories (Ideal, Realistic and Extreme) and add them to the data storage in Algorithmia. The \incode{image\_manipulation.py} script grabs all the selected images from the general dataset containing 95'000 images and moves them to another directory. From there, all images can be easily uploaded to Algorithmia.

At this stage, the \incode{all.txt} file contains all garments worn by image, by identifier (Listing \ref{lst:alltxt}) and the \incode{data.json} with a list of all selected images are needed to generate a new \incode{.json} file. This new file will contain all the image identifiers, and items worn, which will later be reviewed manually. The \incode{scrapping\_data.py} script goes through each image contained in the \incode{data.json} file and matches the id from \incode{all.txt} to get the garments.

\begin{figure}

    \centering
    \begin{lstlisting}[label={lst:alltxt}, caption=all.txt file containing garments]
1 bag
1 shorts
1 t-shirt
2 coat
3 shirt
3 shorts
3 shoes
3 accessories
4 dress
4 purse
4 boots
4 jacket
4 bracelet
4 accessories
5 dress
5 shoes
5 belt
5 purse
\end{lstlisting}


   \begin{lstlisting}[language=json,label={lst:datajson}]
{
    "ideal_cases": [
        {
            "name": "2_coat_400.jpg",
            "articles": ["hat", "coat", "sunglasses"], 
            "amount_people": 1 
        },
        {...},
        {
            "name": "12_silver-asos-dress_400.jpg",
            "articles": [
                "hat",
                "dress",
                "jacket",
                "belt",
                "bag"
            ],
            "amount_people": 1
        }
    ]
}
\end{lstlisting}
\caption{file with chosen images} 

\end{figure}

The main script is \incode{testing\_fashionista.py} which takes the \incode{scrapped\_data.json} images and sends all the images to be evaluated by both algorithms, DeepFashion and Fashion Segmentation, on \gls{algorithmia}. 
% \begin{lstlisting}[language=python, caption=testing\_fashionista.py test\_data function, label={lst:testingalgorithmia}]
% def test_data(dataSetType, algorithm_full, algorithm_name):
%     dataset = (json.loads(open("../scrapped_data.json").read()))
%     with open(algorithm_name+'/'+algorithm_name+'_res.json') as file_already_tested_dataset:
%         already_tested_dataset = (json.loads(file_already_tested_dataset.read()))
      
%     for item in dataset[dataSetType]:
%         img_name = item["name"]
%         # test if data already tested
%         if len(already_tested_dataset[dataSetType])!=0 and any(x["name"] == img_name for x in already_tested_dataset[dataSetType]):
%             continue
%         print("working on image "+ img_name)
%         #algorthmia request
%         input = {
%             "image": "data://fpittet/test/"+img_name
%         }
%         apiKey = [...]
%         client = Algorithmia.client(apiKey)
%         algo = client.algo(algorithm_full)

%         # Algorithmia results
%         print(algo.pipe(input).result)
%         results = (json.loads(open(algorithm_name+'/'+algorithm_name+'_res.json').read()))

%         #save result to json file
%         tmp_data = algo.pipe(input).result
%         tmp_articles = []
%         if algorithm_name == "deepFashion":
%                  output_img_name = tmp_data['output'].split("data://.algo/algorithmiahq/deepFashion/temp/",1)[1]
%             for article in tmp_data['articles']:
%                 print(article['article_name'])          
%                 tmp_articles.append({'article_name': article['article_name'], 'bbox':article['bounding_box'], 'confidence': article['confidence']})
                
%         elif algorithm_name == "fashionSegmentation" :
%             output_img_name = "none" 
%             for article in tmp_data:
%                 print(article['article_name'])
%                 tmp_articles.append({'article_name': article['article_name'], 'bbox':article['bbox'], 'confidence': article['confidence']})
            
        
%         results[dataSetType].append({
%             'name': img_name,
%             'articles': tmp_articles,
%             'output': output_img_name
%         })

%         with open(algorithm_name+'/'+algorithm_name+'_res.json', 'w') as outfile:
%             json.dump(results, outfile)

% \end{lstlisting}

The above function \incode{test\_data} loads the dataset, checks whether the images have already been tested and sends a request to Algorithmia for each image. The input parameters allow testing of different dataset types (Ideal, Realistic, Extreme) and algorithm (Fashion Segmentation and DeepFashion). The results are saved in a new file and directory \incode{[algorithm\_name]/[algorithm\_name]\_res.json}. So DeepFashion's results will be in the \incode{deepFashion} folder under the name \incode{deepFashion\_res.json}.

The next step is to separate all the results into levels. Creating different levels of precision is meant to establish if the algorithm correctly detected a category, but not the most precise clothing item.


\begin{figure}[H]
    \centering
    \includegraphics[width=1\linewidth]{02-main/figures/labels_clothes_categories.png}
    \caption{Clothing and accessories levels of precision}
    \label{fig:clothingLevels}
\end{figure}
For example, if the ground truth is \incode{"button down shirt"} and the first algorithm detects it as \incode{"shirt"} while the second one detects it as \incode{"jacket"}, we need to reflect that the first algorithm got closer to the truth than the second. In order to do so, all item categories were organized by levels (Figure \ref{fig:clothingLevels}). 

This hierarchy is represented in  \incode{articles\_dictionnary\_categories.json} which allows us to create individual result files per algorithm, per level. This job is done by the \incode{script\_res\_levels.py} script. It takes each result file per algorithm and creates new result files per levels (\incode{[algorithm\_name]/[algorithm\_name]\_res\_level[level].json}. Thus, the results for Fashion Segmentation at level 1 will be stored in \incode{fashionSegmentation/fashionSegmentation\_res\_level\_1.json}.

To compare the results, the ground truth data from \incode{scrapped\_data.json} is also converted into different levels while keeping the same naming convention (\incode{groundTruthLevels/ groundTruthLevels\_res\_level\_[level].json}.

All algorithms by levels are then compared to the ground truth to obtain the results in the \incode{algorithmia\_tests\_results.ipynb} notebook.
\section{Results}
To calculate the results, each image needs to have an accuracy percentage. It is calculated by how many correct identifications, misses and false positive were found.

All the code for this section is available in Appendix 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{02-main/figures/calculate_percentage.png}
    \caption{Example of an accuracy percentage calculation}
    \label{fig:accPercentage}
\end{figure}

In the example in Figure \ref{fig:accPercentage}. the ground truth has 4 items, \incode{shoes, dress, hat and bracelet}. The predicted classes were \incode{shoes, dress, bracelet and shirt}. It correctly predicted 3 classes (\incode{shoes, dress and bracelet}) while missing \incode{hat} and falsely identifying a \incode{shirt}. 

To calculate the percentage, we take the correctly identified classes (True Positives) divided by the True Positives plus the False positives and True Negatives. In our cases, this result is a 60\% accuracy rate.

\[Accuracy Rate = \frac{TP}{TP+FP+TN}\]

The results of all the data processing and testing were very interesting.

First of all, the Fashion Segmentation algorithm had the best results overall. The accuracy rates by level and category are shown in Table \ref{tab:res_norm}

\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{@{}cccclll@{}}
\toprule
\textbf{} &
  \multicolumn{3}{c}{\textbf{Deep Fashion}} &
  \multicolumn{3}{l}{\textbf{Fashion Segmentation}} \\ \midrule
\textbf{Category} &
  1 &
  2 &
  3 &
  1 &
  2 &
  3 \\ \midrule
Ideal &
  41.7 &
  26.12 &
  24.6 &
  68.7 &
  59.2 &
  59.2 \\
Realistic &
  24 &
  14.6 &
  13.7 &
  41.7 &
  38 &
  38 \\
Extreme &
  \multicolumn{1}{l}{33} &
  \multicolumn{1}{l}{22.33} &
  \multicolumn{1}{l}{22.33} &
  63 &
  53 &
  53 \\ \midrule
\multicolumn{1}{l}{\textbf{Average per Level}} &
  \textbf{34.1} &
  \textbf{21.3} &
  \textbf{20.1} &
  \multicolumn{1}{c}{\textbf{57.4}} &
  \multicolumn{1}{c}{\textbf{50.3}} &
  \multicolumn{1}{c}{\textbf{50.3}} \\ \midrule
\multicolumn{1}{l}{\textbf{Average per Algorithm}} &
  \multicolumn{3}{c}{\textbf{25.1}} &
  \multicolumn{3}{c}{\textbf{52.7}} \\ \bottomrule
\end{tabular}%
}
\caption[Percentage results per Algorithm per levels]{Percentage results per Algorithm per levels} \label{tab:res_norm}
\end{table}

The average per level percentage is calculated by the weighted average of the ideal, realistic and extreme categories. The average by algorithm is just a simple average between levels.


In both algorithm, levels 2 and 3 have similar results. This is due to the fact that the results given by the algorithm is not very precise in the first place. Deep Fashion does return more precise results but they are not necessarily correct. There was a difference in accuracy rate between levels 2 and 3.

To make the decision between both algorithms, another factor needed to be taken into account: how many images were guessed completely right. To do so, all accuracy rates of images equal to 100\% have been counted. The results are shown in Table \ref{tab:res_allTrue}.

\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{@{}ccccccc@{}}
\toprule
\textbf{}         & \multicolumn{3}{c}{\textbf{Deep Fashion}} & \multicolumn{3}{l}{\textbf{Fashion Segmentation}}                     \\ \midrule
\textbf{Category} & 1             & 2           & 3           & \multicolumn{1}{l}{1} & \multicolumn{1}{l}{2} & \multicolumn{1}{l}{3} \\ \midrule
Ideal             & 4.7           & 0           & 0           & 19.3                  & 0                     & 0                     \\
Realistic         & 7.7           & 0           & 0           & 14.4                  & 1.9                   & 1.9                   \\
Extreme           & 0             & 0           & 0           & 20                    & 0                     & 0                     \\ \midrule
\multicolumn{1}{l}{\textbf{Average per Level}}     & \textbf{5.7} & \textbf{0} & \textbf{0} & \textbf{17.3} & \textbf{0.8} & \textbf{0.8} \\ \midrule
\multicolumn{1}{l}{\textbf{Average per Algorithm}} & \multicolumn{3}{c}{\textbf{1.9}}       & \multicolumn{3}{c}{\textbf{6.3}}            \\ \bottomrule
\end{tabular}%
}\caption[Percentage results of all true predictions per Algorithm per levels]{Percentage results of all true predictions per Algorithm per levels} \label{tab:res_allTrue}
\end{table}

Once again, Fashion Segmentation managed to fully identify more images correctly than Deep Fashion. Even if the percentages are low (6.3 \% vs 1.9\%), completely predicting items worn in an image fully depends on the initial dataset. If the dataset is too precise, the algorithm might miss items. Whereas if it's too lacking, it might identify items that are correct, but not listed in the ground truth. This is why manual input while creating the dataset was necessary. 

After looking more closely at the results by the algorithm, all identified items included a confidence level (from 0 to 1). What would happen if we were to only pick items with confidences from 0.4 to 0.7? Tables \ref{tab:res_percentage_con4_5}, \ref{tab:res_percentage_con6} and \ref{tab:res_percentage_con7} show the results in the percentages while changing this parameter.
\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccccccc}
\hline
\textbf{Confidence level: 0.4 and 0.5}                     & \multicolumn{3}{c}{\textbf{Deep Fashion}}     & \multicolumn{3}{l}{\textbf{Fashion Segmentation}} \\ \hline
\textbf{Category} & 1     & 2     & 3     & 1     & 2    & 3    \\ \hline
Ideal             & 41.68 & 26.12 & 24.6  & 68.7  & 59.2 & 59.2 \\
Realistic         & 23.95 & 14.61 & 13.68 & 41.69 & 38   & 38   \\
Extreme           & 33.16 & 22.33 & 22.33 & 63.67 & 53   & 53   \\ \hline
\multicolumn{1}{l}{\textbf{Average per Level}}     & \textbf{34.1} & \textbf{21.3} & \textbf{20.0} & \textbf{57.5}   & \textbf{50.3}  & \textbf{50.3}  \\ \hline
\multicolumn{1}{l}{\textbf{Average per Algorithm}} & \multicolumn{3}{c}{\textbf{25.1}}             & \multicolumn{3}{c}{\textbf{52.7}}                 \\ \hline
\end{tabular}%
}\caption[Percentage results with confidence levels < 0.4 and 0.5]{Percentage results with confidence levels < 0.4 and 0.5} \label{tab:res_percentage_con4_5}
\end{table}


\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccccccc}
\hline
\textbf{Confidence level: 0.6}                     & \multicolumn{3}{c}{\textbf{Deep Fashion}}     & \multicolumn{3}{l}{\textbf{Fashion Segmentation}} \\ \hline
\textbf{Category} & 1     & 2     & 3     & 1     & 2    & 3    \\ \hline
Ideal             & 33.7  & 22.72 & 21.63 & 68.7  & 59.2 & 59.2 \\
Realistic         & 22.09 & 14.6  & 13.7  & 41.7  & 38   & 38   \\
Extreme           & 26.2  & 22    & 22    & 63.67 & 53   & 53   \\ \hline
\multicolumn{1}{l}{\textbf{Average per Level}}     & \textbf{28.7} & \textbf{19.4} & \textbf{18.4} & \textbf{57.5}   & \textbf{50.3}  & \textbf{50.3}  \\ \hline
\multicolumn{1}{l}{\textbf{Average per Algorithm}} & \multicolumn{3}{c}{\textbf{22.1}}             & \multicolumn{3}{c}{\textbf{52.7}}                 \\ \hline
\end{tabular}%
}
\caption[Percentage results with confidence levels < 0.6]{Percentage results with confidence levels < 0.6} \label{tab:res_percentage_con6}
\end{table}

\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccccccc}
\hline
\textbf{Confidence level: 0.7}                     & \multicolumn{3}{c}{\textbf{Deep Fashion}}     & \multicolumn{3}{l}{\textbf{Fashion Segmentation}} \\ \hline
\textbf{Category} & 1     & 2    & 3    & 1     & 2    & 3    \\ \hline
Ideal             & 27.8  & 16.3 & 13.7 & 68.7  & 59.2 & 59.2 \\
Realistic         & 16.25 & 17.8 & 10.8 & 41.7  & 38   & 38   \\
Extreme           & 17.8  & 22   & 14.5 & 63.67 & 53   & 53   \\ \hline
\multicolumn{1}{l}{\textbf{Average per Level}}     & \textbf{22.7} & \textbf{17.1} & \textbf{12.5} & \textbf{57.5}   & \textbf{50.3}  & \textbf{50.3}  \\ \hline
\multicolumn{1}{l}{\textbf{Average per Algorithm}} & \multicolumn{3}{c}{\textbf{17.5}}             & \multicolumn{3}{c}{\textbf{52.7}}                 \\ \hline
\end{tabular}%
}
\caption[Percentage results with confidence levels < 0.7]{Percentage results with confidence levels < 0.7} \label{tab:res_percentage_con7}
\end{table}
We can see that Deep Fashion does not benefit from changing the confidence levels. Whereas Deep Fashion shows changes in table \ref{tab:res_percentage_con6} as the confidence level is increased to 0.6. Deep Fashion seems to correctly identify clothing but is more modest in its confidence.

Fashion Segmentation does not benefit at all from moving the confidence levels. It seems that it only returns results if it is absolutely sure of its prediction.

This analysis better highlights the two different approaches. Deep Fashion is less confident in its results, even if it might be right, whereas Fashion Segmentation gives higher confidence levels in general.
% In order to create an architecture for our final application, an analysis with has already been researched and developed is necessary. 

% \section{Related work} \label{relatedWork}
% Several research papers are related to the intended application. For example, a team of researchers have created  a model to generate outfits based on a wardrobe \cite{craft}. Others approached the hair segmentation and classification \cite{hair}.

% The following Table~\ref{tab:rp} contains all research papers related to this project.

% \begin{table}[H]
% 	\footnotesize
% 	\centering
% 	\begin{tabular}{ p{2.5cm}|p{2cm}|p{2cm}|p{1.5cm}|p{1.5cm}|p{2.5cm} }
% 		\toprule
% 		\textbf{Title} & \textbf{Company/ research Facility} & \textbf{Authors} &  \textbf{Average h-index} & \textbf{Publication year} & \textbf{Algorithm(s) used}
% 		\\
% 		\midrule
% 		CRAFT: Complementary Recommendation by Adversarial Feature Transform \cite{craft} & Amazon (lab126) & Cong Phuoc Huynh, Arridhana Ciptadi, Ambrish Tyagi, Amit Agrawal &  24.5 & 2018 & Generative transformer network \\
% 		\hline
% 	    Dep Learning for Automated Tagging of Fashion Images \cite{deeplearning} & Amazon & Patricia Gutierrez, Pierre-Antoine Sondag, Petar Butkovic, [...], Arne Knudson & NA & 2018 & \acrshort{cnn} \\
% 		\hline
% 		Creating Capsule Wardrobes from Fashion Images \cite{capsule} & University of Texas (Austin) and Facebook AI Research & Wei-Lin Hsiao, Kristen Grauman & NA & 2017 & \acrshort{knn}\\
% 		\hline
% 		Fashion-Gen: the generative Fashion Dataset and Challenge \cite{fashiongen} & Element AI, MMILA-University of Montreal, Ecole Polytechnique of Montreal & Negar Rostamzadeh, [...], Chris Pal & 22 & 2018 & Generative models\\
% 		\hline
% 		A deep learning approach to hair segmentation and color extraction from facial images \cite{hair} & Technical University of Cluj-Napoca and Babes-Bolyai University, Romania & Diana Borza, Tudor Ileni, Adrian Darabant & 6.5 & 2018 & Deep learning\\
% 		\hline
% 		Regression-Based Landmark Detection on Dynamic Human Models \cite{landmarkDetection} & Korea Advanced Institute of Science and Technology (KAIST) & Deok-Kyeong Jang and Sung-Hee Lee & NA & 2017 & Statistical regression model \\
% 		\bottomrule 
% 	\end{tabular}
% 	\caption[Table of research papers]{Table of research papers} \label{tab:rp}
% 	\label{tab:research}
% \end{table}

% The "Average h-index" has been calculated by averaging the h-index\footnote{The h-index of a publication is the largest number h such that at least h articles in that publication were cited at least h times each. For example, a publication with five articles cited by, respectively, 17, 9, 6, 3, and 2, has the h-index of 3. Source: scholar.google.com} of the first and last mentioned author from their respective Google Scholar pages. In some cases, one of the values was unknown, which is illutrated by an "NA" annotation.

% \section{Convolutional Neural Networks}
% Convolutional Neural Networks, or \acrshort{cnn}, are commonly used to classify images, cluster them by similarity and perform object recognition within scenes\footnote{source: skymind.ai/wiki/convolutional-network}. Considering our needs for image recognition, CNNs are perfectly suited for this implementation.

% Some research papers have implemented CNNs in their work \cite{deeplearning}.

% \section{Transfer Learning}
% Based on a Kaggle video\footnote{www.youtube.com/watch?v=mPFq5KMxKVw} explaining Transfer learning, this machine learning method is used when an existing model has learning capabilities for a certain problem while wishing to apply it to another similar one. 

% Instead of creating a whole new model with a new data set, Transfer learning allows you to change the last layer of a model which is in charge of predictions. This last layer will be trained on the new problem while still using data and layers from the original model.

% This is a cost and time saving approach as it allows developers to leverage existing models and adapt them to their own, custom needs. 

% In our case, Transfer learning can be used in our modules using existing works to fit them to our needs, depending on our needs. 

% \section{Conclusion}
% The research process has allowed us to explore existing works. Surprisingly, a considerable amount of researchers, particularly at Amazon, have explored the possibilities of applying machine learning to fashion. This allows us to work in an existing territory with good foundations and room for improvement.

