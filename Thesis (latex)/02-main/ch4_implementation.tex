\chapter{Implementation}
\label{chap:implementation}
Following the conception and design process, the following infrastructure was implemented:
\section{Back-end and REST API}
As per the architecture diagram \ref{fig:architecturediagram}, the API receives an image, sends it to a \gls{minio} storage, while evaluating it through \gls{openpose} and the \gls{fashionsegmentation} algorithms. It then returns a list of clothing items that can be modified by the user.

A second request is sent with the \incode{requestId} which allows the API to retrieve the image, and calculate the score by sending it to the fashion conscious model. 

The API itself has been implemented using the Python \gls{flask}. An SQL database would have been too heavy to use with non relational data. A noSQL database allows us to easily store and retrieve all the stored data with less overhead. \gls{mongodb} was chosen as it is very stable, has a Python API and is easily deployable.

To store the images, a \gls{minio} cloud storage server was deployed as this type of Object Storage. It is perfect for storing unstructured data such as images. It is lightweight and also has an API and a robust Python SDK to work with the \gls{flask} Server.

\section{Telegram Bot}
\gls{telegram} offers an API for developers which allow them to add functionality to the application, such as Telegram Bots.

Bots act just like a person in the user's contact list. They can interact with it. 

A mock-up example has been created to establish the different messages and interactions between the user and the Bot (Figure \ref{fig:bot_mockup}).

The development has been made using \gls{node} and \gls{telegraf}. Therefore, we can easily interact with users with the Telegram app, while conserving the context of a conversation. This allows us to create a step-by-step interaction with the user.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.7\columnwidth]{02-main/figures/telegram_bot_modelisation.png} 
	\caption{Telegram Bot Mock-up}
	\label{fig:bot_mockup} 
\end{figure}

\section{Machine Learning Model}

In order to predict a fashionability score on an image, a machine learning model needs to be trained. Thus, using an existing MobilNet \cite{tl_keras_mobilnet} in combination with Transfer Learning is a good option.

\subsection{Dataset}
The dataset created to test the Algorithmia models (\fullref{chap:validation}) can not be used to train a model as it only has 271 images. In order to train, validate and test a new model, at least 2500 images are needed.

Therefore, 3000 images were randomly selected out of the 100'000 images of the Chictopia dataset \cite{chictopia}. The initial scripts used in \fullref{chap:validation} were modified to also scrap social data such as likes, comments and favorites.

While retrieving the comments, it was important to check their "sentiment". If negative comments were present, they could not be treated as a positive data point for the image. Hence, the Google Natual Language API \cite{googlesentiment} was used to run a sentiment analysis on all the comments of the 3000 images.

The results concluded that the comments of 2587 images were positive, and only 2 were negative. It was then possible to assume that comments were positive, since 99.92\% were evaluated as such.

The next step in the dataset preparation was to find a way to attribute a score to the images based on the social information, numbers of comments, likes and favorites.

In our opinion, liking an image takes less user effort than commenting on one, while marking an image as a favorite could be considered in between.

The formula created to generate a score based on this data is:

\[ S = 2C + 1.5F + V \]

Where $S$ is the Score, $C$ is the number of Comments, $F$ the number of Favorites and $V$ the number of Votes.

This score would then need to be normalized based on the date of publication. Since an image that has been online longer is more likely to have more social interactions, the calculated score was adjusted:

\[S_{dateNorm} = 100 \frac{S}{D}\]

Where $S_{dateNorm}$ is the new date based normalized Score, $S$ is the original Score, and $D$ the number of days since publication.

The last step was to do a Min Max normalization to create scores from 1 to 10. The following Min-Max Normalization formula was applied:

\[ S_{minMaxNorm} = \frac{S_{dateNorm} - min}{max - min}\]

Where $S_{minMaxNorm}$ is the Min-Max based normalized score, $S_{dateNorm}$ the previously calculated score, $min$ the smallest score found in the dataset, and $max$ the highest.

This last step resulted in the following data distribution:

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\columnwidth]{02-main/figures/ML/data_norm1.JPG} 
	\caption{Data distribution}
	\label{fig:normdata1} 
\end{figure}

The rare highest scores were grouping most of the other scores at class \incode{1}. This class represented 1931 images out of 2185 images (88.38\%). The way the data was distributed made it impossible at this stage to train a model without removing extreme cases.

The best distribution was obtained by changing the images with a score above \incode{1} to a \incode{1}. Meaning that an imagine with the class \incode{8} would be normalized to a \incode{1}. With this final step, the data distribution was acceptable:

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.95\columnwidth]{02-main/figures/ML/data_norm2.JPG} 
	\caption{Data distribution with adjustments to extreme cases}
	\label{fig:normdata2} 
\end{figure}

Its implementation is part of a general script to prepare the data for the training. The file 
\incode{create\_dataset.py} contains all the functions related to the data preparation.

\begin{lstlisting}[language=python, caption=Function to calculate scores]
def calculateScores():
    dataset = (json.loads(open("complete_dataset.json").read()))
    base_date = datetime.strptime("2015-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')
    new_data_set = []
    maxScore = 0
    minScore = 30000
    for image in dataset:
        if image['social'] and image['publishedDate'] and image.get("img_name"):
            comments = int(image['social']['comments'])
            favorites = int(image['social']['favorites'])
            votes = int(image['social']['votes'])
            date = datetime.strptime(image['publishedDate'], '%Y-%m-%d %H:%M:%S')
            days = (base_date - date).days
            # print(days)
            #function = 2*c + v + 1.5*f
            score = (2*comments) + votes + (1.5 * favorites)
            norm_score = score / days *100
            img_name = image['img_name'].split('/')[-1]
            new_img = {
                "id" : image['id'],
                "img_name" : img_name,
                "articles" : image['articles'],
                "score" : norm_score,
                "url": image['url'],
                "img_url": image['img_name']
            }
            new_data_set.append(new_img)
            if norm_score > maxScore:
                maxScore = norm_score
            if norm_score < minScore:
                minScore = norm_score

    for image in new_data_set:
        interimScore = ((image['score']-minScore)/(maxScore-minScore))*10
        normScore = 0
        if interimScore > 1:
            normScore = 1
        else:
            normScore = ((interimScore)/1)*10
        image['norm_score'] = normScore
        image['norm_score_category'] = int(normScore)+1
    with open('data-set/data_set_withScores.json', 'w') as outfile:
            json.dump(new_data_set, outfile)
\end{lstlisting}

\subsection{Training}
The provided Python Notebook\footnote{The original Python Notebook was provided by Professor Jean Hennebert, \acrshort{icossys}} contained a MobileNet model that could be trained on images in folders named after the class, each containing the images.

The dataset was randomly split between the Train (70\%), Validate (20\%) and Test (10\%) sections, resulting in the following file structure:

\dirtree{%
.1 dataset/.
.2 train/.
.3 1/.
.3 2/.
.3 [...].
.3 10/.
.2 validate/.
.3 1/.
.3 2/.
.3 [...].
.3 10/.
.2 test/.
.3 1/.
.3 2/.
.3 [...].
.3 10/.
}

While running the training with the default settings, the training was taking approximately 2 minutes per Epochs, meaning that with a maximum of 1000 Epochs, the time needed to finish this task would be over 2 days. 

The need to use Tensorflow-GPU with the graphics card was necessary to test multiple layers and parameters to find the best fit. \gls{cuda} needed to be installed as it can run on a GTX 1070, the available graphics card on the development machine. 

The setup process was tedious but worth it. The training time per Epoch dropped from 2 minutes to 10 seconds. Even though the improvements between Epochs did not move after approximately 16 Epochs, taking the time to install \gls{cuda} proved to still be worth it.

The final selected model consists of the layers shown below. All training iterations with results can be found in Appendix IV. A comparison of parameters between training iterations can be found in Table \ref{tab:comp_params_ml_train} The model is originally a pre-trained MobilNet with some added layers.

\begin{lstlisting}[language=json,caption=Final FashionAI Keras Model Summary]
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
mobilenetv2_1.00_224 (Model) (None, 7, 7, 1280)        2257984   
_________________________________________________________________
global_average_pooling2d (Gl (None, 1280)              0         
_________________________________________________________________
dense (Dense)                (None, 16)                20496     
_________________________________________________________________
dropout (Dropout)            (None, 16)                0         
_________________________________________________________________
activation (Activation)      (None, 16)                0         
_________________________________________________________________
dense_1 (Dense)              (None, 10)                170       
_________________________________________________________________
activation_1 (Activation)    (None, 10)                0         
=================================================================
Total params: 2,278,650
Trainable params: 2,244,538
Non-trainable params: 34,112
_________________________________________________________________
\end{lstlisting}

\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccccccc}
\hline
\textbf{iteration n°} & \textbf{Dense} & \textbf{Batchnorm} & \textbf{dropout} & \textbf{early stopping} & \textbf{optimizers.Adam} & \textbf{accuracy} \\ \hline
1 & 16 & n & 0.5 & 25  & 0.0007 & 38 \\ \hline
2          & 16 & n & 0.7 & 100 & 0.0009 & 42 \\ \hline
3          & 32 & y & 0.6 & 50  & 0.0009 & 35 \\ \hline
4          & 32 & n & 0.5 & 50  & 0.0005 & 34 \\ \hline
\end{tabular}%
} \caption[Comparison of parameters between training iterations]{Comparison of parameters between training iterations} \label{tab:comp_params_ml_train}
\end{table}


\section{Testing}

The Telegram bot is functional and can be tested. Several images were sent to test:
\begin{itemize}
    \item the bot: does it return the correct messages ? what happens if there is an error with the back-end?
    \item the back-end: can it correctly save the images and information onto the Object storage and database ? 
    \item ML Model: how bias is it when sending images taken in a normal environment (smartphone, in front of a mirror for example) ?
\end{itemize}

\subsection{Telegram Bot}

The bot responded as expected and handled errors from the back-end, such as no human detected on an image, or no clothing identified. It would also handle the case where the back-end was inaccessible.

What still needs improving is unexpected user input when the clothing items can be modified. A more intuitive approach to this step would also be preferred, such as selecting correctly identified clothing, rather than rewriting a list as text.

\begin{figure}
\centering
\begin{minipage}{.55\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{02-main/figures/bot_screenshot_1.JPG}
    \centering
    % Predicted score: 3/10
    % \subcaption{ Original Image.}

\end{minipage}%
\begin{minipage}{.55\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/bot_screenshot_2.JPG}
        % Predicted score: 3/10
        % \subcaption{ Thin body type alteration}

\end{minipage}

\caption{Screenshots of interactions with the FashionAI Telegram bot}
\label{fig:screenshotBots}
\end{figure}

\subsection{Back-end}

The API has no issues regarding error messages handling. What could be improved though are:

\begin{itemize}
    \item checking input clothing items: the back-end could check whether the items its receiving correspond to clothing. For example, it would refuse an item such as "plant" but allow "heels".
    \item the response time: this API calls a total of three Machine learning algorithms, which takes time. A fully in-house approach with servers equipped with GPUs made for machine learning, such as a Titan RTX \footnote{https://www.nvidia.com/en-us/deep-learning-ai/products/titan-rtx/}, would lower the compute times and give a faster response.
\end{itemize}

\subsection{Machine Learning model}

A few images not in the original dataset have been used to test the model. They all come from the author's personal picture collection. 

As the results were surprising and the scores were mostly very low. Some alterations to the images were done to see what parameters might affect the output score.

Firstly, the background has been removed and changed in Figures \ref{fig:testingMlBackgrounds}. It seems that the background does come into play regarding the predictions. Since the dataset used for training has backgrounds of different varieties, it is a given that this variable impacts the final score. It is also possible that figure \ref{fig:testingMlBackgrounds} (c) 's score is the same as (b)'s because the Model saw that the background was added manually.

\begin{figure}
\centering
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{02-main/figures/ML/test_slim_3_o.jpg}
    \centering
    Predicted score: 3/10
    \subcaption{ Original Image.}

\end{minipage}%
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/test_slim_3_backgroundRemoval.jpg}
        Predicted score: 1/10
        \subcaption{ Background removal}

\end{minipage}
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/test_slim_3_backgroundChange.jpg}    Predicted score: 1/10
        \subcaption{ Background change.}
\end{minipage}
\caption{Testing images with background changes}
\label{fig:testingMlBackgrounds}
\end{figure}



Another aspect that can be tested is the person's body type. Does this affect the score? Surprisingly, it does, but not in the way we might think. Figures \ref{fig:testingMlbodySize} give an interesting outcome. The highest score of 9/10 is given to the larger body type image. It is still important to keep in mind that these alterations done with Adobe Photoshop are not perfect and can include small distortions that might affect the model that have nothing to do with the body size. In this case, slimming down the person in the image does not have an affect on the score, but enlarging it does.



\begin{figure}
\centering
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{02-main/figures/ML/img2_slim_o.jpg}
    \centering
    Predicted score: 3/10
    \subcaption{ Original Image.}

\end{minipage}%
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/img2_slimmer_m.jpg}
        Predicted score: 3/10
        \subcaption{ Thin body type alteration}

\end{minipage}
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/img2_fat_m.jpg}    Predicted score: 9/10
        \subcaption{ Larger body type alteration.}
\end{minipage}
\caption{Testing images with body types changes}
\label{fig:testingMlbodySize}
\end{figure}

Lastly, we wanted to take a look at how brightness and contrast affect the score. In Figures \ref{fig:testingMlcontrast}, altering the original image's contrast and brightness in both directions seem to drastically lower the score. Once again, this is most likely due to the training dataset.

Images with higher social interactions could have a higher production value, thus better lighting and contrast adjustments in the first. Resulting in the model categorising bad lighting as a poor score.
\begin{figure}
\centering
\begin{minipage}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{02-main/figures/ML/img4_lighting_o.jpg}
    \centering
    Predicted score: 4/10
    \subcaption{ Original Image }

\end{minipage}%
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/img4_lighting_contrast_brightness_high.jpg}
        Predicted score: 1/10
        \subcaption{ High contrast and brightness}

\end{minipage}
\begin{minipage}{.33\textwidth}
  \centering
   \includegraphics[width=.95\linewidth]{02-main/figures/ML/img4_lighting_contrast_brightness_low.jpg}
        Predicted score: 1/10
        \subcaption{ Low contrast and brightness}

\end{minipage}
\caption{Testing images with contrast and brightness changes}
\label{fig:testingMlcontrast}
\end{figure}


With these small testing samples, we are able to conclude that the original training dataset is not diverse enough to accurately predict a score based on an image. It is a good proof-of-concept but does yet depict reality.

The training dataset was based on social engagement to calculate the score, the more comments, likes and favorites, the higher the score. The issue with this approach is that "just because it is popular, it does not mean that it is the best".

In order to improve the results, a better normalization must be done to completely remove extreme cases (very high engagement). The model does have a tendency to classify an image with a lower score, as most of the training data are examples of lower scores. A bigger dataset might also help obtain a better distribution (c.f. Figure \ref{fig:normdata2}).
 
 Regarding the professional aspect of the image, the dataset should have been normalized even further. The background should have been removed to make sure that the model was being trained only on the outfits. 
 
 The dataset \textbf{must} be improved to make sure that body shape, camera angles and setting, as well as skin tone don't affect the results to this extent.
 
 Obviously, it is normal to assume that a yellow dress might fit a darker tone person more than a lighter one. The body type must also be taken into account when dealing with clothing with specific fits, such as bustiers.
 
 Also, the clothing items detected in the images and included in the dataset are \textbf{not} used to train the model at this time. It could be a good extra training parameter for the model to have this information and could improve the accuracy rates drastically.
% Following the research process, it was decided to develop  two modules:
% \begin{itemize}
%     \item Object detection module to detect a person in an image
%     \item Body Type detection module to determine a person's body type and measurements
% \end{itemize}

% Considering the lack of existing work regarding body type detection from images, this module was chosen as it is one of the most challenging and needs the most research. Other modules have existing papers and/or code archives that prove their feasibility. Whereas, the body type detection module's algorithms and implementations are unkown.

% \label{chap:implementation}
% \section{Object detection module}
% Using the \Gls{Tensorflow} library and their examples \footnote{github.com/tensorflow/models}, we were able to use their example research notebooks to test different models with our three testing images.

% This library uses \gls{coco} and \Gls{Tensorflow}. 

% \subsection{Microsoft COCO: Common Objects in Context}

% This dataset is ideal for object and \gls{keypoints} detection \cite{coco}. It contains over 330'000 images (more than 200'000 are labeled) with categories and captions. For \gls{keypoints} detection, in contains 250'000 people with their \gls{keypoints}. This makes COCO a great library to work with. 

% It also allows developers to contribute to the dataset if they want to train on a certain type of object \footnote{www.immersivelimit.com/tutorials/create-coco-annotations-from-scratch}. The use of the \incode{json} format is intuitive.

% Each image contains the width, height, image url and an id. It also contains secondary fields such as license, flickr URL and date captured. 

% The most interesting part in this dataset is the annotations. It contains a list of points, an area, whether it's a crowd or not, a bounding box, an id and the category id. The list of points forms an exact outline of the detected object and the bounding box is the coordinates of the box that surrounds this outline. Crowd annotations are used when too many objects of the same sort is in a section of an image. For example, the picture of a stadium with a cheering crowd. In this case, the segmentation points are \Gls{runlengthencoding}.

% The \gls{keypoints} detection format contains \gls{keypoints} in the categories. For example, the  "person" category has "nose", "left eye", etc. as \gls{keypoints}. It also contains the coordinates of the skeleton to identify what is connected to what (left ankle to left knee for example). The annotations are similar to the object detection, with some extra fields such as the number of \gls{keypoints} and the \gls{keypoints} themselves. 

% This intuitive and simple way of describing images makes COCO an ideal dataset for object and \gls{keypoints} detection.

% \subsection{Tensorflow}
% \Gls{Tensorflow} is an open source platform for machine learning containing models that are ready to use. It contains \gls{Keras} which is a standard in machine learning. 

% \subsection{Implementation}
% The first model we can choose is \incode{ssd\_mobilenet\_v1\_coco\_2017\_11\_17} which creates a bounding box around an object and identifies it.

% Our results for this first model work nicely with our testing image as they are already cropped (Figure \ref{fig:ssdmobilenetcocores}). Another series of images was used to order to determine its success with a different environment and with other zooms (Figure \ref{fig:ssdmobilenetcocoresvar}). 

% In the first two images, the system manages to detect the correct bounding box despite the noisy environment. The last image contains a lot of noise and three people. The model only detected two of them. In any case, our goal is to generate bounding boxes around \emph{one} unique person in the image.



% \begin{figure}

% \centering
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p1_model1_objdetection.PNG}

% \end{minipage}%
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p2_model1_objdetection.PNG}
    
% \end{minipage}
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p3_model1_objdetection.PNG}
   
% \end{minipage}
% \caption{SSD Mobilenet COCO dataset testing results}
% \label{fig:ssdmobilenetcocores}
% \end{figure}

% % \begin{figure}

% % \centering
% % \begin{minipage}{.33\textwidth}
% %   \centering
% %   \includegraphics[width=.95\linewidth]{02-main/figures/i1_model1_objdetection.PNG}

% % \end{minipage}%
% % \begin{minipage}{.33\textwidth}
% %   \centering
% %   \includegraphics[width=.95\linewidth]{02-main/figures/i2_model1_objdetection.PNG}
    
% % \end{minipage}
% % \begin{minipage}{.33\textwidth}
% %   \centering
% %   \includegraphics[width=.95\linewidth]{02-main/figures/i3_model1_objdetection.PNG}
   
% % \end{minipage}
% % \caption{SSD Mobilenet COCO dataset testing results with variety}
% % \label{fig:ssdmobilenetcocores}
% % \end{figure}

% \begin{figure}
% \centering
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i1_model1_objdetection.PNG}
%      \subcaption{Image 1}
% \end{minipage}%
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i3_model1_objdetection.PNG}
%      \subcaption{Image 2}
% \end{minipage}
% \begin{minipage}{1\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i2_model1_objdetection.PNG}
%      \subcaption{Image 3}
% \end{minipage}
% \caption{SSD Mobilenet COCO dataset testing results with variety}
% \label{fig:ssdmobilenetcocoresvar}
% \end{figure}
% The other included model is the \incode{mask\_rcnn\_inception\_v2\_coco\_2018\_01\_28} which highlights and contours the detected object.

% \begin{figure}

% \centering
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p1_model2_objdetection.PNG}

% \end{minipage}%
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p2_model2_objdetection.PNG}
    
% \end{minipage}
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/p3_model2_objdetection.PNG}
   
% \end{minipage}
% \caption{Mask RCNN inception COCO dataset testing results}
% \label{fig:maskrcnncocores}
% \end{figure}

% In Figure \ref{fig:maskrcnncocores}, the model detects and highlights a human multiple times over the same image. It seems that since the image is already cropped, it is detecting several times different areas of the body. In Figure \ref{fig:maskrcnncocoresvar}, the first two pictures are correctly colored in. With the first model, we had issues with the last image, whereas this model is able to identify all three people in this noisy image.

% \begin{figure}
% \centering
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i1_model2_objdetection.PNG}
%   \subcaption{Image 1}
   
% \end{minipage}%
% \begin{minipage}{.5\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i3_model2_objdetection.PNG}
%     \subcaption{Image 2}
% \end{minipage}
% \begin{minipage}{1\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/i2_model2_objdetection.PNG}
%     \subcaption{Image 3}
% \end{minipage}
% \caption{Mask RCNN inception COCO dataset testing results with variety}
% \label{fig:maskrcnncocoresvar}
% \end{figure}




% \section{Body type detection module}

% In order to classify body types, the first thing to do is to get the measurements. From this, the body can be identified.

% \subsection{Measurements}
% This part of the module has to calculate in pixels the different measurements based on a bounding boxed image.

% Some attempts for body measurements have been accomplished with green screen segmentation \footnote{github.com/ankesh007/Body-Measurement-using-Computer-Vision/blob/master/Presentation.pdf} but the need for a general background is important in our application. For this reason, we looked into body pose estimation examples from \gls{opencv} which detects \gls{keypoints} and landmarks on pictures. From these points, the width in pixels can be determined.

% For this development, we have used \gls{opencv} and \gls{coco} glossary. Using the sample code from OpenCV \footnote{github.com/CMU-Perceptual-Computing-Lab/openpose}, we selected 4 \gls{keypoints} (right shoulder, left shoulder, right hip, left hip) to be traced out on the image. Ideally, we would like to have measurements for the shoulders, bust, waist and hips. These can be calculated based on the hips and shoulders points and the person's outline.

% The code included three different datasets to determine the \gls{keypoints}; \gls{coco}, \Gls{mpi} and Body\_25.


% \begin{figure}
% \centering
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/body_estimation_coco_dataset.PNG}
%     \subcaption{ COCO dataset}
%     \label{fig:cocobodyestim}

% \end{minipage}%
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/body_estimation_mpi_dataset.PNG}
%     \subcaption{MPI dataset}
%     	\label{fig:mpibodyestim}

% \end{minipage}
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{02-main/figures/body_estimation_body_25_dataset.PNG}
%     \subcaption{Body\_25 dataset}
%     	\label{fig:body25bodyestim}

% \end{minipage}
% \caption{Keypoints detection}
% \end{figure}

% Each dataset generates a list of points with their coordinates on the image \incode{(x,y)}. To get the width, the two X values must be subtracted. \[W = |X_i - X_j|\].

% With this particular image, the results were the following:

% \begin{table}[H]
% 	\footnotesize
% 	\centering
% 	\begin{tabular}{c c c}
% 		\toprule
% 		\textbf{dataset} & \textbf{Shoulders (px)} & \textbf{Hips (px)} \\
% 	 	\midrule
% 	    COCO & 225 & 140 \\
% 		\hline
% 	     MPI & 282 & 112 \\
% 		\hline
% 	    Body\_25 & 225 & 140 \\
% 		\bottomrule 
% 	\end{tabular}
% 	\caption[Results of body estimation]{Results of body estimation}
% 	\label{tab:res_bodytype}
% \end{table}

% Both Body\_25 and \gls{coco} have the same values. Their keypoints are the same, contrary to \gls{mpi}. Nethertheless, the difference between the numbers is inferior to 5\%.

% Our calculations are only based on the X coordinate, meaning that if the person on the image is not standing straight, the results will not be correct. 

% An euclidean distance between the two points must be calculated. The \gls{numpy} \incode{np.linalg.norm} formula was used to calculate the euclidean distance between two points.

% The results show that only the hips with the \gls{mpi} dataset changed. As seen in Figure \ref{fig:mpibodyestim}, the hips are not on the same Y axis.


% \begin{table}[H]
% 	\footnotesize
% 	\centering
% 	\begin{tabular}{c c c}
% 		\toprule
% 		\textbf{dataset} & \textbf{Shoulders (px)} & \textbf{Hips (px)} \\
% 	 	\midrule
% 	    COCO & 225 & 140 \\
% 		\hline
% 	     MPI & 282 & \textbf{117.95} \\
% 		\hline
% 	    Body\_25 & 225 & 140 \\
% 		\bottomrule 
% 	\end{tabular}
% 	\caption[Results of body estimation with euclidean distances]{Results of body estimation with euclidean distances}

% \end{table}

% \begin{lstlisting}[language=Python, caption=Distances between keypoints]
% shoulders = abs(points[BODY_PARTS["LShoulder"]][0] - points[BODY_PARTS["RShoulder"]][0])
% hips = abs(points[BODY_PARTS["LHip"]][0] - points[BODY_PARTS["RHip"]][0])

% print('Shoulders: '+ str(shoulders), 'Hips: ' + str(hips))

% shouldersL = np.array((points[BODY_PARTS["LShoulder"]][0] ,points[BODY_PARTS["LShoulder"]][1]))
% shouldersR = np.array((points[BODY_PARTS["RShoulder"]][0] ,points[BODY_PARTS["RShoulder"]][1]))

% shouldersEucl = np.linalg.norm(shouldersL - shouldersR)

% hipsL = np.array((points[BODY_PARTS["LHip"]][0] ,points[BODY_PARTS["LHip"]][1]))
% hipsR = np.array((points[BODY_PARTS["RHip"]][0] ,points[BODY_PARTS["RHip"]][1]))

% hipsEucl = np.linalg.norm(hipsL - hipsR)

% print('Shoulders Eucl: '+ str(shouldersEucl), 'Hips Eucl: ' + str(hipsEucl))


% \end{lstlisting}

% \subsection{Classification} \label{chap:classification}
% This section of the module takes the measurement from the previous section and should classify the body type.

% Multiple approaches can be chosen in order to get a body type based on these measurements.

% \begin{itemize}
%     \item Machine Learning approach with a mapping function. This would be the ideal solution but would require a large dataset with measurements and ground truths to create a use-able training set, which we do not have.
%     \item Algorithmic approach. This is the best choice considering the data and time available.
% \end{itemize}

% First of all, since so many different calculations exist on how to determine a body type, we have chosen the one that uses shoulders, bust, waist and hips measurements \footnote{www.whowhatwear.co.uk/how-to-find-body-shape-calculator}. From their instructions on how to determine your body type, formulas and if statements have been created.

% \begin{figure}[H] 
% 	\centering 
% 	\includegraphics[width=0.5\columnwidth]{img/howtomeasure.jpg} 
% 	\captionsource{How to measure yourself}{ How to measure yourself }{www.whowhatwear.co.uk}
% 	\label{fig:howtomeasure} 
% \end{figure}

% Inverted triangle if \[ ( shoulders | bust) / hips > 1.05 \]
% Rectangle if 
% \begin{gather*}
% MAX (shoulders, bust, hips) = t \\
% MIN (shoulders, bust, hips) = x \\
% MID (shoulders, bust, hips) = y \\
%  (x  AND  y) > t*0.95 
% \end{gather*}
% % \[ waist / (shoulders | bust) < 1.25 && shoulder / bust < 1.05 && shoulder / hips < 1.05 \]

% Pear if \[ Hip / (shoulders | bust) > 1.05 \] 

% Hourglass if 
% \begin{gather*}
%  MAX(hips, shoulders) = t \\
%  MIN(hips, shoulders) = y \\
%  y > t*95 
% \end{gather*}
% % \[ Waist / MAX ( shoulders,bust) < 1.25 \]
% % \[AND waist / hips < 1.25 \]
% % \[ AND shoulder / hips < 1.05 \]

% After creating these statements, the collected data has been used to determine manually which body type they are.

% \begin{table}[H]
% 	\footnotesize
% 	\centering
% 	\begin{tabular}{p{3cm}|p{2cm}|p{2cm}|p{2cm} }
% 		\toprule
% 		\textbf{Body part} & \textbf{person 1} & \textbf{person 2} & \textbf{person 3} \\
% 	 	\midrule
% 	    \textbf{Shoulders} & 109 & 107 & 107 \\
% 		\hline
% 	    \textbf{Bust} & 86 & 82 & 93 \\
% 		\hline
% 		\textbf{Waist} & 83 & 94 & 94 \\
% 		\hline
% 		\textbf{Hips} & 93 & 102 & 110 \\
% 		\bottomrule 
% 	\end{tabular}
% 	\caption[Body measurements per subject]{Body measurements per subject} 
% 	\label{tab:res_bodytypes_measurements}
% \end{table}

% \begin{figure}
% \centering
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{img/bodies/person1.jpg}
%     \centering
%     \subcaption{ Person 1}

% \end{minipage}%
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{img/bodies/person2.jpg}
%         \subcaption{ Person 2}

% \end{minipage}
% \begin{minipage}{.33\textwidth}
%   \centering
%   \includegraphics[width=.95\linewidth]{img/bodies/person3.jpg}
%         \subcaption{ Person 3}
% \end{minipage}
% \caption{Pictures of subjects}
% \end{figure}

% Using these measurements and the conditional statements created, we calculated for each all the ratios to determine which person has which body type.

% \begin{table}[H]
% 	\footnotesize
% 	\centering
% 	\begin{tabular}{p{3cm}|p{2cm}|p{2cm}|p{2cm} }
% 		\toprule
% 		\textbf{Results} & \textbf{person 1} & \textbf{person 2} & \textbf{person 3} \\
% 	 	\midrule
% 	    \textbf{Inverted triangle} & \textcolor{mygreen}{\textbf{yes}} & \textcolor{myred}{no} & \textcolor{myred}{no} \\
% 		\hline
% 	    \textbf{Rectangle} & \textcolor{myred}{no} & \textcolor{myred}{no} & \textcolor{myred}{no} \\
% 		\hline
% 		\textbf{Pear} & \textcolor{mygreen}{\textbf{yes}} & \textcolor{mygreen}{\textbf{yes}} & \textcolor{mygreen}{\textbf{yes}} \\
% 		\hline
% 		\textbf{Hourglass} & \textcolor{myred}{no} & \textcolor{mygreen}{\textbf{yes}} & \textcolor{mygreen}{\textbf{yes}} \\
% 		\bottomrule 
% 	\end{tabular}
% 	\caption[Body types per subject]{Body types per subject} 
% 	\label{tab:res_bodytypes_bool}
% \end{table}

% At this point, a main issue appeared. From this small dataset, it is very difficult to understand whether the formulas are wrong, or if our test subjects are too similar. As we can see in table \ref{tab:res_bodytypes_bool}, the results from person 2 and 3 are the same. This is understandable considering the very close results in body measurements.

% Another issue we have from these conditional statements is that the results are Boolean. Thus, as we can see from the results, a person can have two body types while not keeping track of \textbf{a matching score} to a body type. If a person is 60\% hourglass but 80\% Pear, an algorithm could be implemented to favor the higher score. In our case though, each person matches to 2 body types, all them pears. From this, we have determined that subject 1 is most likely an inverted triangle, person 2 and 3 are Hourglasses.

% Therefore, another system needs to be put in place to determine a body type with a matching score. The issue is that no scoring method exists and no official way of determining a body type exists. This makes this process much more difficult to implement considering these results.

% Ideally, a dataset containing more diverse types of body shapes could help determining if the conditional statements created above can be implemented. Also, validating the data with an image consultant would help to determine if our method is correct.