import json
import requests
import urllib.request
from bs4 import BeautifulSoup
import csv
import dateparser
from datetime import date, datetime
import operator
import os
import os.path
import shutil
import os
import itertools
import urllib.request
import random

def calculateScores():
    dataset = (json.loads(open("complete_dataset.json").read()))
    # print(dataset[0])
    base_date = datetime.strptime("2015-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')
    # print(base_date)
    new_data_set = []
    maxScore = 0
    minScore = 30000
    
    for image in dataset:
       
        if image['social'] and image['publishedDate'] and image.get("img_name"):
            
            comments = int(image['social']['comments'])
            favorites = int(image['social']['favorites'])
            votes = int(image['social']['votes'])
            date = datetime.strptime(image['publishedDate'], '%Y-%m-%d %H:%M:%S')
            days = (base_date - date).days
            # print(days)
            #function = 2*c + v + 1.5*f
            score = (2*comments) + votes + (1.5 * favorites)
            norm_score = score / days *100
            img_name = image['img_name'].split('/')[-1]
            new_img = {
                "id" : image['id'],
                "img_name" : img_name,
                "articles" : image['articles'],
                "score" : norm_score,
                "url": image['url'],
                "img_url": image['img_name']
            }
            new_data_set.append(new_img)
            if norm_score > maxScore:
                maxScore = norm_score
            
            if norm_score < minScore:
                minScore = norm_score

            # print(date)
            # maxScore = max(new_data_set, key=new_data_set['score'])
            
    print(maxScore)
    print(minScore)
    
    # min max normalization
    for image in new_data_set:
        interimScore = ((image['score']-minScore)/(maxScore-minScore))*10
        normScore = 0
        if interimScore > 1:
            normScore = 1
        
        else:
            normScore = ((interimScore)/1)*10

        image['norm_score'] = normScore
        image['norm_score_category'] = int(normScore)+1
    
    with open('data-set/data_set_withScores.json', 'w') as outfile:
            json.dump(new_data_set, outfile)

def download(filename, url ,destDir):

  # filename is (post_id)_(image_name)
#   filename = img[0]+'_'+img[1].split('/')[-1]
    image_filename = os.path.join(destDir, filename)
    print("Fetching '%s' to '%s'..." % (filename, image_filename))

    urllib.request.urlretrieve(url, image_filename)

# photo_dir = os.path.join(os.getcwd(), 'photos')
# images = [[(line.rstrip().split("\t")[0], line.rstrip().split("\t")[1]) for line in open(os.path.join(photo_dir, f))] for f in os.listdir(photo_dir) if f.endswith('.txt')]
# images = list(itertools.chain.from_iterable(images))

# print('Downloading %s images to "./images" directory...' % len(images))

# image_dir = os.path.join(os.getcwd(), 'images')
# if not os.path.exists(image_dir):
#   os.makedirs(image_dir)


def moveImagesToDirectory():
    dataset = (json.loads(open("data-set/data_set_withScores.json").read()))
    destDir =  "G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/ml_data/"
    errors = []
    for image in dataset:
        img_name = str(image['id'])+"_"+image['img_name']
        destDirFull = destDir+str(image['norm_score_category'])+"/"
        if not os.path.isfile(destDirFull+img_name): 
            try:                
                download(img_name, image['img_url'],destDirFull)
                #     shutil.copy(srcDir+str(image['id'])+"_"+image['img_name'], destDir+str(image['norm_score_category'])+"/", follow_symlinks=True)
            except:
                print("error with image "+ img_name)
                errors.append(image['id'])
        else:
            print("image "+img_name+" already downloaded")
    print("Done , errors with images:")
    print(errors)
# moveImagesToDirectory()

def seperateDataSet():
    train = 70
    validate = 20
    dirs = [1,2,3,4,5,6,7,8,9,10]
    dirPath = "G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/ml_data/"
    destPath = "G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/ml_data_split/" 
    for dir in dirs:
        
        n_files = sum([len(files) for r, d, files in os.walk(dirPath+str(dir))])-1
        # print("files for "+str(dir)+": "+ str(n_files))
        trainByClassAmount = int(n_files*train/100)
        validateByClassAmount =  int(n_files*validate/100)


        fileNamesInDir = os.listdir(dirPath+str(dir))
        random.shuffle(fileNamesInDir)
        fileNamesInDir.remove("desktop.ini")
        trainImageNames = fileNamesInDir[:trainByClassAmount]
        validateImageNames = fileNamesInDir[trainByClassAmount:trainByClassAmount+validateByClassAmount]
        testImageNames = fileNamesInDir[trainByClassAmount+validateByClassAmount:]

        # copy images
        print("train: "+str(len(trainImageNames)))
        for trainImg in trainImageNames:
            # print(trainImg)
            shutil.copy(dirPath+str(dir)+"/"+trainImg, destPath+"train/"+str(dir)+"/", follow_symlinks=True)
        trainDirNames = os.listdir(destPath+"train/"+str(dir)+"/")
        if "desktop.ini" in trainDirNames: trainDirNames.remove("desktop.ini")

        if not len(trainDirNames) == len(trainImageNames):
            print(set(trainDirNames)-set(trainImageNames))
            print("In Train, class"+ str(dir)+" moved "+str(len(trainDirNames))+" files instead of"+str(len(trainImageNames)))
            
        print("validate : "+str(len(validateImageNames)))
        for validateImg in validateImageNames:
            # print(validateImg)
            shutil.copy(dirPath+str(dir)+"/"+validateImg, destPath+"validate/"+str(dir)+"/", follow_symlinks=True)
        validateDirNames = os.listdir(destPath+"validate/"+str(dir)+"/")
        if "desktop.ini" in validateDirNames: validateDirNames.remove("desktop.ini")

        if not len(validateDirNames) == len(validateImageNames):
            print("In Validate, class"+ str(dir)+" moved "+str(len(validateDirNames))+" files instead of"+str(len(validateImageNames)))
            print(set(validateDirNames)-set(validateImageNames))

        print("test : "+str(len(testImageNames)))
        for testImg in testImageNames:
            # print(testImg)
            shutil.copy(dirPath+str(dir)+"/"+testImg, destPath+"test/"+str(dir)+"/", follow_symlinks=True)
        testDirNames = os.listdir(destPath+"test/"+str(dir)+"/")
        if "desktop.ini" in testDirNames: testDirNames.remove("desktop.ini")
        if not len(testDirNames) == len(testImageNames):
            print("In Test, class"+ str(dir)+" moved "+str(len(testDirNames))+" files instead of"+str(len(testImageNames)))
            print(set(testDirNames)-set(testImageNames))
        #test
            
           
           
    
           
            
           
 
seperateDataSet()
# calculateScores()



