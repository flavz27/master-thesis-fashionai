from uuid import uuid4
from pymongo import collection
from datetime import datetime
import json


class Image:
    def __init__(self,
                 ip_address: str,
                 mongo_collection: collection,
                 minio_id: str,
                 uploaded_at: datetime = datetime.utcnow()):
        self.id = None
        self.ip_address = ip_address,
        self.minio_id = minio_id,
        self.uploaded_at = uploaded_at
        self.mongo_collection = mongo_collection

    def save(self):
        self.id = self.mongo_collection.insert_one({
            "ip_address": self.ip_address,
            "minioDbId": self.minio_id,
            "date_uploaded": self.uploaded_at
        }).inserted_id
        return self.id

class Article:
    def __init__(self,
                 articles: list,
                 mongo_collection: collection,
                 minio_id: str,
                 score: int,
                 uploaded_at: datetime = datetime.utcnow()):
        self.id = None
        self.score = score
        self.articles = articles,
        self.minio_id = minio_id,
        self.uploaded_at = uploaded_at
        self.mongo_collection = mongo_collection

    def save(self):
        self.id = self.mongo_collection.insert_one({
           "articles": self.articles,
           "score": self.score,
            "minioDbId": self.minio_id,
            "date_uploaded": self.uploaded_at
        }).inserted_id
        return self.id
