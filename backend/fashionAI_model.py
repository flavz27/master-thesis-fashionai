from __future__ import print_function

import tensorflow as tf
from tensorflow.keras.models import load_model
import os
import numpy as np
import itertools
import random
import base64
import io

#test
from PIL import ImageFile
from keras.preprocessing import image
#base 64 img ?
def calculateScore(imageB64):
    fashionAiModel = load_model('MN_V2_224_TF02.h5')
    img_width, img_height = 224, 224
    # Decoding and pre-processing base64 image
    img = image.img_to_array(image.load_img(io.BytesIO(base64.b64decode(imageB64)), target_size=(img_width, img_height))) / 255.

    img = img.astype('float16')

    x = np.expand_dims(img, axis=0)
    res = fashionAiModel.predict_classes(x)
    print(res[0])
    return res[0]+1

