import uuid
import datetime
from minio import Minio
from minio.error import ResponseError
from pymongo import MongoClient
import requests
import Algorithmia
from pymongo.errors import PyMongoError
import base64
from Image_custom import Image, Article
import json
from flask import Flask, request, jsonify, abort

import fashionAI_model

import random
import os

app = Flask(__name__)


@app.route('/api/')
def hello():
    return "FashionAI API"
   
@app.route('/test/api/image', methods=['POST'])
def test_post_image():
    req_res = {
                "req_id": "1b0173f2-95fe-4e3a-93bc-800f584ed826",
                "articles": ["pants", "shoes", "vest", "footwear"]
            }
           
    return req_res

@app.route('/test/api/getScore/<requestId>', methods=['POST'])
def testSetScore(requestId):
    if not requestId == "1b0173f2-95fe-4e3a-93bc-800f584ed826":
        return {"error":"wrong requestId"}, 400

    itemList =  request.get_json(silent=True)['articles']
    if not itemList:
        return {"error":"can't get articles"}, 400
    score = int(random.randint(1, 10))
    req_res = {
        "score": score
    }
    return req_res
"""
Route in POST an image and saves it to minio. It also has to store metadata from this image into mongo (ip address from user)
"""
@app.route('/api/image', methods=['POST'])
def post_image():
    # 1. save image to minio
    file = request.files['image']
    pos = file.tell()
    file.seek(0, 2)  # seek to end
    size = file.tell()
    file.seek(pos)  # back to original position
    print("image size"+str(size))

    if file is not None:
        print("recieved image")
        
        minio_client = Minio('',
                             access_key='',
                             secret_key='',
                             secure=False)
        mongo_client = MongoClient("mongodb://localhost:27017/")
        db = mongo_client.fashionai

        try:
            # 1. Save images in minio
            # https://docs.min.io/docs/python-client-api-reference.html
            minioId = str(uuid.uuid4())
            # img_extension = 
            # img_name = minioId+
            data = minio_client.put_object('fashionai', minioId, file, size)
 

            # 2. Save data in mongo
            # https://api.mongodb.com/python/current/tutorial.html
            new_image = Image(
                ip_address=request.remote_addr,
                mongo_collection=db.images,
                minio_id=minioId
            )
            new_image.save()

            print("saved to minio: "+minioId )
            # 3. send image to pose estimation
            minDataPoints = ['Neck', 'LKnee', 'RKnee']
            url = "https://icoservices.k8s.tic.heia-fr.ch/poseestim/estimations/file?keypoints=body&threshold=0.05&ret_img=false"
            file.seek(pos)
            files = {'image': file}
       
            r = requests.post(url, files=files)

            if r.status_code == 504:
                return {"error":"server issue"}, 400

            posteEstimationRes = r.json()
            posteEstimationPpl = posteEstimationRes['data']['people']
            print("request to pose estimation done")
            if not posteEstimationPpl:
                return {"error":"no human detected"}, 400
                # abort(400, "no human detected")
            
            datapoints = []
            for point in posteEstimationPpl[0]['body']:
                if not point['confidence'] == 0:
                    datapoints.append(point['key'])

            print(datapoints)
            minDetectedPoints = set(datapoints)&set(minDataPoints)
            if len(minDetectedPoints) < 2:
                return {"error":"not enough points in image"}, 400
                     
            # check if the result is ok, minimum: face + feet ?
            # 4. send to algorithmia
            print("sending to algorithmia")
            file.seek(pos)
            image_b64 = base64.b64encode(file.read()).decode('utf-8')
            input = {
                "image": image_b64
            }
            apiKey = ""
            client = Algorithmia.client(apiKey)
            algo = client.algo("/tofriede/FashionSegmentation/0.1.5")

            # Algorithmia results
            print(algo.pipe(input).result)
            results = algo.pipe(input).result

            articles = []

            for article in results:
                print(article['article_name'])
                articles.append(article['article_name'])
            
            articles = list(set(articles)) #unique
            req_res = {
                "req_id": new_image.minio_id,
                "articles": articles
            }

         
            return req_res

        except ResponseError as err:  # minio error
            print(err)
        except PyMongoError as err:  # mongo error
            print(err)

    else:
        return "No image sent", 400


@app.route('/api/getScore/<requestId>', methods=['POST'])
def getScore(requestId):
    minio_client = Minio('',
                        access_key='',
                        # TODO : clean this, remove keys from code
                        secret_key='',
                        secure=False)
    itemList =  request.get_json(silent=True)
    if not itemList:
        return {"error":"can't get articles"}, 400

    mongo_client = MongoClient("mongodb://localhost:27017/")
    db = mongo_client.fashionai

    
    # itemList =  request.get_json(silent=True)['articles']
    image = minio_client.get_object('fashionai',requestId)
    print(len(image.data))
    
    image64 = base64.b64encode(image.data)
    # score = int(random.randint(1, 10))
    # 
    
    # print(image64)
    score = fashionAI_model.calculateScore(image64)
    
    req_res = {
        "score": int(score)
    }
    new_articles = Article(
            articles = itemList['articles'],
            score = int(score),
            mongo_collection=db.articles,
            minio_id=requestId
        )
    new_articles.save()

    return jsonify(req_res)
    #get image from request id in minio
    #give the image to the .h5 model to get score
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

