from flask_mongoengine import MongoEngine

db = MongoEngine()


def init_db(app):
    global db
    app.config['MONGODB_SETTINGS'] = {
        'db': 'fashionai',
        'host': ''
    }
    db.init_app(app)
