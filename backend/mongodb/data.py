from flask_mongoengine import *
from ._base import db


class Image(db.Document):
    ip_address = db.StringField(required=True)
    date_uploaded = db.DateTimeField()
    minioDbId = db.StringField()

    meta = {'collection': 'image'}


class Article(db.Document):
    score = db.IntField(required=True)
    date_uploaded = db.DateTimeField()
    articles = db.ListField()
    minioDbId = db.StringField()

    minioDbId = db.StringField()

    meta = {'collection': 'image'}
