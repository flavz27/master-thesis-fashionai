import json
import os
import os.path

def moveImgs(datasettype):
    srcDir = "C:/Users/flavz/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/images/"
    destDir =  "C:/Users/flavz/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/dataset/"+datasettype+"/"
    data = (json.loads(open("../scrapped_data.json").read()))
    for item in data[datasettype]:
        name = item["name"]
        if os.path.isfile(srcDir+name): 
            os.replace(srcDir+name, destDir+name)
        else: 
            print("image "+name+" not found, most likely already moved")

    print("done")

moveImgs("extreme_cases")

def noiseImage(file):
    print("noising image "+file)

def lowerResolution(file):
    print("lowering resolution image "+file)
