import json


def updateFileRes(algorithm_name, level):
    # if datasettype = "all":
   
    currentFileLevelName = algorithm_name+"_res_level"+str(level)+".json"
    currentFileLevelDir =  'C:/Users/flavz/Documents/code/master_thesis/testing/algorithmia/'+algorithm_name+"/"
    # categories = ["realistic_cases", "extreme_cases"]
    categories = ["ideal_cases", "realistic_cases", "extreme_cases"]
    
    dictionary = (json.loads(open('C:/Users/flavz/Documents/code/master_thesis/testing/algorithmia/articles_dictionnary_categories.json').read()))
    # with open(algorithm_name+'/'+algorithm_name+'_res.json') as dataset_file:
    #     dataset = (json.loads(dataset_file.read()))
    fileResWithCategory = {}
    # print(dictionary)
    if algorithm_name == "groundTruthLevels":
        groundTruthDataSet = (json.loads(open('../scrapped_data.json').read()))
        dataset = {}
        for category in categories:
            dataset[category] = []
            print(dataset)
            for item in groundTruthDataSet[category]:
                newItem = {"name":item['name'], "articles":[]}
                for article in item['articles']:
                    newItem['articles'].append({'article_name':article, "confidence": "NA"})
                    print(newItem)
                dataset[category].append(newItem)
    else:
        dataset = (json.loads(open(algorithm_name+'/'+algorithm_name+'_res.json').read()))

    
    for category in categories:
        fileResWithCategory[category] = []
        for item in dataset[category]:
            newItem = {'name': item['name'],'articles':[]}
            for article in item['articles']:
            
 
                for dictionnary_term_lv1 in dictionary:
                    print(article)
                    print("  . ")
                    print(dictionnary_term_lv1['name'])
                    if article['article_name'] == dictionnary_term_lv1['name']:                        
                        newItem['articles'].append({"article_name": dictionnary_term_lv1['name'], "confidence":article['confidence']})             
                    else:
                        for dictionnary_term_lv2 in dictionnary_term_lv1['content']:
                           
                            if article['article_name'] == dictionnary_term_lv2['name']:
                                # print("article "+article+" is in dictionnary")
                                # print(dictionnary_term_lv2)
                                if level == 1:
                                    newItem['articles'].append({"article_name":dictionnary_term_lv1['name'], "confidence":article['confidence']})
                                else:
                                    newItem['articles'].append({"article_name":dictionnary_term_lv1['name'], "confidence":article['confidence']})
                                
                            else:
                                for dictionnary_term_lv3 in dictionnary_term_lv2['content']:
                                    if article['article_name'] == dictionnary_term_lv3['name']:
                                        if level == 1:
                                            newItem['articles'].append({"article_name":dictionnary_term_lv1['name'], "confidence":article['confidence']})
                                        elif level == 2:
                                            newItem['articles'].append({"article_name":dictionnary_term_lv2['name'], "confidence":article['confidence']})
                                        elif level == 3:
                                            newItem['articles'].append({"article_name":article['article_name'], "confidence":article['confidence']})

                                
                                        # print("article "+article+" is in dictionnary")
            fileResWithCategory[category].append(newItem)
            # print(fileResWithCategory['ideal_cases'])                    

    
    # print(fileResWithCategory)

    with open(currentFileLevelDir+currentFileLevelName, 'w') as outfile:
        json.dump(fileResWithCategory, outfile)



levels = [1,2,3]
for level in levels:
    updateFileRes("deepFashion", level)