
import csv
import json
import numpy as np
import requests
import urllib.request
from bs4 import BeautifulSoup
#Dataset types:
# - ideal_cases
# - realistic_cases
# - extreme_cases

def scrap(datasettype):
    dir = 'G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/garments/'
    dataset = (json.loads(open("../scrapped_data.json").read()))  
    with open(dir+'all.txt') as csvfile:
        csv_reader = list(csv.reader(csvfile, delimiter='\t'))
        for item in dataset[datasettype]:
            if "_" in item["name"]:
                current_id = item["name"].split('_')[0]
                print(current_id)
                item['articles'] = []
                for row in csv_reader:
                    # print("row id"+row[0])
                    # print("current id:"+current_id)
                    if row[0] == current_id:
                        item['articles'].append(row[1])

    with open('../scrapped_data.json', 'w') as outfile:
        json.dump(dataset, outfile)

    print("done")  


def getUniqueLabelsFromGroundTruth():
    dir = 'G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/garments/'
    # dataset = (json.loads(open("fashionista_labels.json").read()))  
    with open(dir+'all.txt') as csvfile:
        csv_reader = list(csv.reader(csvfile, delimiter='/t'))
        labels = []
        # flatten_array = csv_reader
        for row in csv_reader:
            labels.append(row[1])
    unique_labels = np.unique(labels)
    print(unique_labels)   
# scrap("realistic_cases")
# getUniqueLabelsFromGroundTruth()

def putInAlphabeticalOrder(filename):
    dictionnary = json.loads(open(filename+".json").read())
    new_dict = []
    for article in dictionnary:
        articles = np.array(article['articles'])
        data = { 
            "category_name": article['category_name'],
            "articles": list(np.sort(articles))
        }
        new_dict.append(data)
        # article_list = 
    print(new_dict)
    with open(filename+".json", 'w') as outfile:
        json.dump(new_dict, outfile)

# putInAlphabeticalOrder("articles_dictionnary_categories")
def getSoup(url):
 
    number = url.split('/')[-1]
    print(number)
    voteId = 'vote_text_'+number
    commentId = 'comment_text_'+number
    favId = 'favorite_text_'+number
    response = requests.get(url)

    soup = BeautifulSoup(response.text, "html.parser")
    if not soup.find(id="title_bar"):
        return {}
    comments = soup.find(id=commentId).text.strip().strip(' comments').rstrip()
    favorites = soup.find(id=favId).text.strip().strip(' favorites').rstrip()
    votes = soup.find(id=voteId).text.strip().strip(' votes').rstrip()

    colorBoxes = soup.find(id='color_boxes').findChildren('a')
    for colorBox in colorBoxes:
        colorBox['href']
        print(colorBox['href'])
    # print(colorBoxes)
    
    return {'comments':comments, 'favorites':favorites, 'votes': votes}

# getSoup('http://www.chictopia.com/photo/show/368429')

def getComments(url):  
    response = requests.get(url)
    comments = []
    soup = BeautifulSoup(response.text, "html.parser")
    if not soup.find(id="title_bar"):
        return {}
    commentsDiv = soup.findAll("div", {"class":"comment_content"})
    for comment in commentsDiv:
        comments.append(comment.text.strip().rstrip())

    return comments

def scrapComments():
    dir = 'G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/posts/'

    with open(dir+'training_dataset.txt') as csvfile:
        csv_reader = list(csv.reader(csvfile, delimiter='\t'))
        items = []     
        print(len(csv_reader)) 
        for row in csv_reader:
            item = {
                'id': row[0]
            }
         
            url = row[1]
            comments = getComments(url)
            item['comments'] = comments
            items.append(item)
            print("id: "+ row[0])
    with open('../scrapped_data_comments.json', 'w') as outfile:
        json.dump(items, outfile)       
scrapComments()

def scrapAdditionalFeaturesFromPost(datasettype):
    dir = 'G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista/posts/'
    dataset = (json.loads(open("../scrapped_data.json").read()))  
    with open(dir+'all.txt') as csvfile:
        csv_reader = list(csv.reader(csvfile, delimiter='\t'))
        for item in dataset[datasettype]:
            if "_" in item["name"]:
                current_id = item["name"].split('_')[0]
                print(current_id)
                item['social'] = []
                for row in csv_reader:
                    # print("row id"+row[0])
                    # print("current id:"+current_id)

                    if row[0] == current_id:
                        url = row[1]
                        print("calling function with url "+ url)
                        social = getSoup(url)
                        item['social'] = social
                        print(social)
    with open('../scrapped_data.json', 'w') as outfile:
        json.dump(dataset, outfile)         
# scrapAdditionalFeaturesFromPost("extreme_cases")

