import Algorithmia
import json
import base64
import PIL.Image
import urllib.request


def test_data(dataSetType, algorithm_full, algorithm_name):
    dataset = (json.loads(open("../scrapped_data.json").read()))
    with open(algorithm_name+'/'+algorithm_name+'_res.json') as file_already_tested_dataset:
        already_tested_dataset = (json.loads(file_already_tested_dataset.read()))
      
    for item in dataset[dataSetType]:
        img_name = item["name"]
        # test if data already tested
        if len(already_tested_dataset[dataSetType])!=0 and any(x["name"] == img_name for x in already_tested_dataset[dataSetType]):
            continue
        print("working on image "+ img_name)
        #algorthmia request
        input = {
            "image": "data://fpittet/test/"+img_name
        }
        apiKey = "simtN2sO+xdyzGdMNr5C3xKyKrQ1"
        client = Algorithmia.client(apiKey)
        algo = client.algo(algorithm_full)

        # Algorithmia results
        print(algo.pipe(input).result)
        results = (json.loads(open(algorithm_name+'/'+algorithm_name+'_res.json').read()))

        #save result to json file
        tmp_data = algo.pipe(input).result
        tmp_articles = []
        if algorithm_name == "deepFashion":
                 output_img_name = tmp_data['output'].split("data://.algo/algorithmiahq/deepFashion/temp/",1)[1]
            for article in tmp_data['articles']:
                print(article['article_name'])          
                tmp_articles.append({'article_name': article['article_name'], 'bbox':article['bounding_box'], 'confidence': article['confidence']})
                
        elif algorithm_name == "fashionSegmentation" :
            output_img_name = "none" 
            for article in tmp_data:
                print(article['article_name'])
                tmp_articles.append({'article_name': article['article_name'], 'bbox':article['bbox'], 'confidence': article['confidence']})
            
        
        results[dataSetType].append({
            'name': img_name,
            'articles': tmp_articles,
            'output': output_img_name
        })

        with open(algorithm_name+'/'+algorithm_name+'_res.json', 'w') as outfile:
            json.dump(results, outfile)

      
#/algorithmiahq/DeepFashion/1.3.0
#/tofriede/FashionSegmentation/0.1.5
test_data("extreme_cases", "/tofriede/FashionSegmentation/0.1.5", "fashionSegmentation")