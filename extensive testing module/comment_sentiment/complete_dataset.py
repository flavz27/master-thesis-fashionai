import json
import requests
import urllib.request
from bs4 import BeautifulSoup
import csv
import dateparser

def getSoup(url):
 
    number = url.split('/')[-1]

    voteId = 'vote_text_'+number
    commentId = 'comment_text_'+number
    favId = 'favorite_text_'+number
    response = requests.get(url)

    soup = BeautifulSoup(response.text, "html.parser")
    if not soup.find(id="title_bar"):
        return {},{}
    comments = soup.find(id=commentId).text.strip().strip(' comments').rstrip()
    favorites = soup.find(id=favId).text.strip().strip(' favorites').rstrip()
    votes = soup.find(id=voteId).text.strip().strip(' votes').rstrip()
    publishedDate = soup.find("div", class_='left px10 italics').text.strip().strip('Updated on ').rstrip()
    # print(publishedDate)
    publishedDate = dateparser.parse(publishedDate)
    # print(publishedDate)
    # colorBoxes = soup.find(id='color_boxes').findChildren('a')
    # for colorBox in colorBoxes:
    #     colorBox['href']

    # print(colorBoxes)
    social = {'comments':comments, 'favorites':favorites, 'votes': votes}
    return social, publishedDate # todo

def completeDataSet():
    dataset = (json.loads(open("./results.json").read()))
    # Web: date, social
    dir = 'G:/Google Drive/MASTER/TM_FlaviaPittet_2019-20/testing_datasets/fashion_dataset-master/fashionista'
    urls = '/posts/training_dataset.txt'
    articles = '/garments/training_dataset.txt'
    img_urls = '/photos/training_dataset.txt'
    
    for item in dataset:
        #URLs
        itemId = item['id']
        with open(dir+urls) as csvfile:
            csv_reader = list(csv.reader(csvfile, delimiter='\t'))
            for row in csv_reader:
                url_id = row[0]
                if itemId == url_id:
                    item['url'] = row[1]
        #articles
        with open(dir+articles) as csvfile2:
            csv_reader2 = list(csv.reader(csvfile2, delimiter='\t'))
            item['articles'] = []
            # print(csv_reader2)
            for row in csv_reader2:
                article_id = row[0]
                if itemId == article_id:
                    item['articles'].append(row[1])
        #image url           
        with open(dir+img_urls) as csvfile3:
            csv_reader3 = list(csv.reader(csvfile3, delimiter='\t'))
     
            # print(csv_reader2)
            for row in csv_reader3:
                article_id = row[0]
                if itemId == article_id:
                    item['img_name'] = row[1]

        social, publishedDate = getSoup(item['url'])
        # print(getSoup(item['url']))
        item['social'] = social
        item['publishedDate'] = str(publishedDate)         
        # print(item)
        with open('./complete_dataset.json', 'w') as outfile:
            json.dump(dataset, outfile)         
    
completeDataSet()