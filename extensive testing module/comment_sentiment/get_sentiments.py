import json
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types
import os

def callGoogleApi(comment, client):
    
    # The text to analyze
    text = comment
    document = types.Document(
        content=text,
        language="EN",
        type=enums.Document.Type.PLAIN_TEXT)

    # Detects the sentiment of the text
    sentiment = client.analyze_sentiment(document=document).document_sentiment
    
    return sentiment.score
def fakeCallGoogleApi(comment, client):
    return 0

def getSentiments():
    credential_path = "G:\Google Drive\MASTER\TM_FlaviaPittet_2019-20\masterthesis-dc80ab8d4336.json"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    # Instantiates a client
    client = language.LanguageServiceClient()

    dataset = (json.loads(open("../scrapped_data_comments.json").read()))
    numberOfComments = 0
    already_analyzed_dataset = (json.loads(open("./results.json").read()))

    for item in dataset:
        if len(already_analyzed_dataset)!=0 and any(x["id"] == item['id'] for x in already_analyzed_dataset):
            continue
        
        # if int(item['id']) > lastId:
        print(item['id'])
        averagePerItem = 0
        numberOfCommentsPerItem = 0
        valuePerItem = 0
        for comment in item['comments']:
            numberOfComments = numberOfComments+1
            numberOfCommentsPerItem = numberOfCommentsPerItem+1
            valuePerItem = valuePerItem+callGoogleApi(comment, client)
        
        if numberOfCommentsPerItem > 0:
            averagePerItem = valuePerItem / numberOfCommentsPerItem
        else: averagePerItem = "NA"
        newItem = {'id':item['id'],'sentiment_score':averagePerItem}
            
        # item['sentiment_score'] = averagePerItem
        # else:
        #     newItem = item
        already_analyzed_dataset.append(newItem)
        with open('./results.json', 'w') as outfile:
            json.dump(already_analyzed_dataset, outfile)
    print(numberOfComments)


# getSentiments()
# 

def getGeneralStats():
    already_analyzed_dataset = (json.loads(open("./results.json").read()))
    statSum = 0
    positiveCount = 0
    negativeCount = 0
    sentiment_scores = []
    numberItems = len(already_analyzed_dataset)

    for item in already_analyzed_dataset:
        score = item['sentiment_score']
        # print(score)
        if not score == "NA":
            score = float(score)
            sentiment_scores.append(score)
            statSum = statSum + score
            if score > 0:
                positiveCount = positiveCount+1
            elif score < 0:
                negativeCount = negativeCount + 1
    generalPercentage = statSum / numberItems
    # sentiment_scores = sentiment_scores.sort()
    # print(sentiment_scores)
    minScore = min(sentiment_scores)
    maxScore = max(sentiment_scores)
    print("general average percentage: "+ str(generalPercentage))
    print("positives: "+str(positiveCount)+" negatives: "+str(negativeCount))
getGeneralStats()