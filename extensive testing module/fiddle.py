
import json

dataset = (json.loads(open("./scrapped_data.json").read()))
already_tested_dataset = (json.loads(open("fashionista/fashionista_res.json").read()))

# print(already_tested_dataset["realistic_cases"])
for item in dataset["realistic_cases"]:
    img_name = item["name"]
    if any(x["name"] == img_name for x in already_tested_dataset["realistic_cases"]):
    # if img_name not in already_tested_dataset["realistic_cases"]:
        print(img_name +" is in dataset")
        
    else:
        # print(img_name +" is not in dataset")
        pass

print()